<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetail extends Model{
	protected $table = 'purchase_order_details';
    public $fillable = ['purchase_order_id','product_service_id','po_number','product_code','qty','uom_id','unit_rice','amount'];

    public function purchaseOrder(){
        return $this->belongsTo('App\PurchaseOrder','purchase_order_id');
    }

   	public function productService(){
        return $this->belongsTo('App\ProductServices','product_service_id');
    }

    public function deliveryOrderDetail(){
        return $this->hasMany('App\DeliveryOrderDetail','purchase_order_detail_id');
    }

    public function uom(){
        return $this->belongsTo('App\Uom','uom_id');
    }
    
}