<?php

	namespace App;
	use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model{
	protected $table = 'purchase_orders';
	public $fillable = ['external_provider_id','po_source','po_number','date','term','attention','reff','spesial_instruction'];
    
	public function externalProvider(){
        return $this->belongsTo('App\ExternalProvider','external_provider_id');
    }

    public function purchaseOrderDetail(){
        return $this->hasMany('App\PurchaseOrderDetail','purchase_order_id');
    }

}
