<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model{

	protected $table = 'delivery_orders';
	protected $fillable = ['do_number','date','external_provider_id','ship_to','remark','receiver','approval'];
    
	public function externalProvider(){
        return $this->belongsTo('App\ExternalProvider','external_provider_id');
    }

    public function deliveryOrderDetail(){
        return $this->hasMany('App\DeliveryOrderDetail','delivery_order_id');
    }

}
