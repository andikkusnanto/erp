<?php
	namespace App;
	use Illuminate\Database\Eloquent\Model;

class AppMenu extends Model{
    protected $table = 'app_menus';
    protected $fillable = ['name','href','description'];

	public function userRoleDetail(){
        return $this->hasMany('App\UserRoleDetail','app_menu_id');
    }

}
