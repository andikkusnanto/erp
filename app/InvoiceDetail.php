<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $table = 'invoice_details';
    public $fillable = ['invoice_id','delivery_order_detail_id','invoice_number','po_number','do_number','product_code','qty','uom_id','unit_price','amount','remark'];
	
    public function invoice(){
        return $this->belongsTo('App\Invoice','invoice_id');
    }

    public function uom(){
        return $this->belongsTo('App\Uom','uom_id');
    }

    public function deliveryOrderDetail(){
        return $this->belongsTo('App\DeliveryOrderDetail','delivery_order_detail_id');
    }

    public function paymentVoucherDetail(){
        return $this->hasMany('App\PaymentVoucherDetail','invoice_id');
    }

    

}
