<?php

	namespace App;
	use Illuminate\Database\Eloquent\Model;

class PaymentVoucher extends Model
{
    protected $table = 'payment_vouchers';
    public $fillable = ['pv_number','external_provider_id','date','status'];

    public function externalProvider(){
		return $this->belongsTo('App\ExternalProvider','external_provider_id');
	}

	public function paymentVoucherDetail(){
		return $this->hasMany('App\PaymentVoucherDetail','payment_voucher_id');
	}

}
