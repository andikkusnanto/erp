<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

class DeliveryOrderDetail extends Model{
	protected $table = 'delivery_order_details';
    protected $fillable = ['delivery_order_id','purchase_order_detail_id','product_service_id','do_number','po_number','product_code','qty','uom_id','remark'];
	
    public function deliveryOrder(){
        return $this->belongsTo('App\DeliveryOrder','delivery_order_id');
    }

   	public function purchaseOrderDetail(){
        return $this->belongsTo('App\PurchaseOrderDetail','purchase_order_detail_id');
    }
    
    public function invoiceDetail(){
        return $this->hasMany('App\InvoiceDetail','delivery_order_detail_id');
    }

    public function uom(){
        return $this->belongsTo('App\Uom','uom_id');
    }

    public function productService(){
        return $this->belongsTo('App\ProductServices','product_service_id');
    }

}