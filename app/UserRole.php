<?php

    namespace App;
    use Illuminate\Database\Eloquent\Model;

class UserRole extends Model{
    protected $table = 'user_roles';
    protected $fillable = ['role','description'];
    
	public function userRoleDetail(){
        return $this->hasMany('App\UserRoleDetail','user_role_id');
    }

    public function user(){
        return $this->hasMany('App\User','user_role_id');
    }

 }
