<?php

	namespace App;
	use Illuminate\Database\Eloquent\Model;

class Invoice extends Model{
	protected $table = 'invoices';
	public $fillable = ['external_provider_id','invoice_number','date','remark'];

	public function externalProvider(){
		return $this->belongsTo('App\ExternalProvider','external_provider_id');
	}

	public function invoiceDetail(){
		return $this->hasMany('App\InvoiceDetail','invoice_id');
	}
}
