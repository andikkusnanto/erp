<?php
    namespace App;
    use Illuminate\Database\Eloquent\Model;

class ExternalProvider extends Model{
	protected $table = 'external_providers';
    public $fillable = ['expcode','expname','pic','phone','email','maps','address'];

	public function purchaseOrder(){
        return $this->hasMany('App\PurchaseOrder','external_provider_id');
    }

	public function invoice(){
        return $this->hasMany('App\Invoice','external_provider_id');
    }

    public function paymentVoucher(){
        return $this->hasMany('App\PaymentVoucher','external_provider_id');
    }

}
