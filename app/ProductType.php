<?php

	namespace App;
	use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'product_types';
    public $fillable = ['type_name','description'];
    
	public function productServices(){
        return $this->hasMany('App\ProductServices','product_type_id');
    }
}
