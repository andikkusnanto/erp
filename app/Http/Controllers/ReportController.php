<?php

  namespace App\Http\Controllers;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Redirect;
  use App\Http\Controllers\Controller;
  use App\PurchaseOrder;
  use App\PurchaseOrderDetil;
  use App\ExternalProvider;
  use App\ProductServices;
  use App\Uom;
  use Session;
  use Illuminate\Support\Facades\View;
  use Illuminate\Support\Facades\Input;
  use DB;
  use App\Http\Requests\PurchaseOrderRequest;

class ReportController extends Controller{
    public function financialReport(Request $request){
        $financial_report_summary=DB::select('select * from(select purchase_orders.po_number,purchase_orders.date as po_date,product_services.product_name, purchase_order_details.amount as po_amount, delivery_order_details.do_number, delivery_orders.date as do_date,delivery_order_details.remark as do_remark, invoice_details.invoice_id,invoice_details.invoice_number, invoices.date as invoice_date, invoice_details.amount as invoice_amount, payment_voucher_details.ID as pv_id,payment_voucher_details.pv_number, payment_voucher_details.amount as pv_amount,payment_voucher_details.remark as pv_remark, payment_vouchers.date as pv_date
        from (((((((purchase_orders LEFT JOIN purchase_order_details ON purchase_orders.ID = purchase_order_details.purchase_order_id) LEFT JOIN delivery_order_details ON purchase_order_details.ID = delivery_order_details.purchase_order_detail_id) LEFT JOIN invoice_details ON delivery_order_details.ID = invoice_details.delivery_order_detail_id) LEFT JOIN payment_voucher_details ON invoice_details.invoice_id = payment_voucher_details.invoice_id) LEFT JOIN payment_vouchers ON payment_voucher_details.payment_voucher_id = payment_vouchers.ID) LEFT JOIN invoices ON invoice_details.invoice_id = invoices.ID) LEFT JOIN delivery_orders ON delivery_order_details.delivery_order_id = delivery_orders.ID) LEFT JOIN product_services ON purchase_order_details.product_service_id = product_services.ID)TBL01 Order By po_number asc');

        return View::make('layouts/dashboard',array())->nest('content', 'reports.orderreport',array('data'=>$financial_report_summary));
    }
}
