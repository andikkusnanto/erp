<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\User;
    use App\UserRole;
    use App\UserRoleDetil;
    use App\AppMenu;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use Session;
    use Illuminate\Support\Facades\Redirect;
    use DB;

class UserRoleManController extends Controller{
     public function getUserMenu(Request $request){
        $email = Input::get('email');
        $user_menu=DB::SELECT('select * from(SELECT users.email, user_roles.ID, user_roles.role, user_role_details.app_menu_id, app_menus.name as menu_name, app_menus.href, app_menus.description, user_role_details.privileges
            FROM ((users LEFT JOIN user_roles ON users.user_role_id = user_roles.ID) LEFT JOIN user_role_details ON user_roles.ID = user_role_details.user_role_id) LEFT JOIN app_menus ON user_role_details.app_menu_id = app_menus.ID)TBL01 WHERE email ="'.$email.'" order By menu_name ASC');

        foreach($user_menu as $row){
            $array[] = array(
                'email'=>$row->email,
                'role'=>$row->role,
                'menu_name'=>$row->menu_name,
                'href'=>$row->href,
                'description'=>$row->description,
                'privileges'=>$row->privileges
                );
        }
        echo json_encode($array);
    }

    public function index(Request $request){
        $users = User::all();
        $user_roles = UserRole::all();
        $app_menus = AppMenu::all();
        $options = array(
            'users'=>$users,
            'user_roles'=>$user_roles,
            'app_menus'=>$app_menus,
        );
        return view::make('layouts/dashboard',array())->nest('content', 'userrolemans.index',$options);
    }

    public function create(){
        $user_role = UserRole::all();
        $options = array(
            'user_role'=>$user_role,
            );
        return view::make('layouts/dashboard',array())->nest('content', 'userroles.create',$options);
    }

    public function show(Request $request, $id){
        $user_role = UserRole::findOrFail($id);
        $user_role_detail = UserRoleDetail::where('user_role_id','=',$id)->get();
        $app_menus = AppMenu::all();
        $options = array(
            'user_role'=>$user_role,
            'user_role_detail'=>$user_role_detail,
            'app_menus'=>$app_menus,
        );
        return view::make('layouts/dashboard',array())->nest('content', 'userroles.show',$options);
    }

    public function edit(Request $request, $id){
        $user_role = UserRole::findOrFail($id);
        $user_role_detail = UserRoleDetail::where('user_role_id','=',$id)->get();
        $app_menu = AppMenu::all();

        $options = array(
            'user_role'=>$user_role,
            'user_role_detail'=>$user_role_detail,
            'app_menu'=>$app_menu,
        );
        return view::make('layouts/dashboard',array())->nest('content', 'userroles.edit',$options);
    }

    public function destroy($id){
        try{
            $user = User::where('email',$id)->delete();
            Session::flash('flash_message', 'Data has been deleted.');
            return redirect()->route('userroleman.index');
        }                   
        catch(\Exception $e){
            return redirect()->route('userroleman.index')->with('flash_warning','Data has been deleted.');
        }
    }

    public function getAppMenu(){
        $q = Input::get('q');
        $app_menu = AppMenu::where('name', 'like', '%'.$q.'%')->orderBy('name', 'asc')->limit(10)->get();
        $array = array();
        foreach($app_menu as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->name
                );
        }
        echo json_encode($array);
    }

    public function getSave(Request $request){
        $idx= Input::get('counterx');

        for($i=0;$i<=$idx-1;$i++){
            $email = Input::get('email'.$i);
            $user_role_id = Input::get('user_role_id'.$i);
            $user_role =UserRole::where('id','=',$user_role_id)->first();
            $check_record = User::where('email','=',$email)->first();

            if(empty($check_record)){
            }else{
                $user = User::where('email','=',$email)->first();
                $user->user_role_id =$user_role_id; 
                $user->role=$user_role->role;
                $user->save();
            }
        }

        $users = User::all();
        $user_roles = UserRole::all();
        $app_menus = AppMenu::all();
        $options = array(
            'users'=>$users,
            'user_roles'=>$user_roles,
            'app_menus'=>$app_menus,
        );
        return view::make('layouts/dashboard',array())->nest('content', 'userrolemans.index',$options);
    }
}