<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Http\Requests\ProductRequest;
    use App\Http\Controllers\Controller;
    use App\ProductServices;
    use App\ProductType;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use DB;

class ProductController extends Controller
{
    public function index(Request $request){
        $product_services = ProductServices::all();
        return View::make('layouts/dashboard',array())->nest('content', 'products/index',array('data'=>$product_services));
    }

    public function create(Request $request){
        $product_services = ProductServices::all();
        $product_types = ProductType::all();
        $options = array(
            'product_services'=>$product_services,
            'product_types'=>$product_types, 
            );
        return View::make('layouts/dashboard',array())->nest('content', 'products.create',$options);
    }

    public function store(Request $request){
        ProductServices::create($request->all());
        return redirect()->route('product.index')->with('success','Item created successfully');
    }

    public function show($id){
        $product_service = ProductServices::findOrFail($id);
        return View::make('layouts/dashboard',array())->nest('content', 'products.show',array('data'=>$product_service));
    }                                                                                                       

    public function edit(Request $request, $id){
        $product_service = ProductServices::findOrFail($id);
        $product_types = ProductType::all();
        $options = array(
            'product_service'=>$product_service,
            'product_types'=>$product_types,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'products.edit',$options);
    }

    public function update(ProductRequest $request, $id){
        ProductServices::findOrFail($id)->update($request->all());
        return redirect()->route('product.index')->with('success','Item updated successfully');
    }

    public function destroy($id){
        $product_service = ProductServices::findOrFail($id)->delete();
        return redirect()->route('product.index')->with('success','Item deleted successfully');
    }

    public function search(Request $request){
        $query = $request->get('search');   
        $product_service = ProductServices::where('product_name', 'LIKE', '%' . $query . '%')->orWhere('spesification', 'LIKE', '%' . $query . '%')->orWhere('product_code', 'LIKE', '%' . $query . '%')->orderBy('product_type_id', 'asc')->orderBy('product_name', 'asc')->paginate(10);
        return View::make('layouts/dashboard',array())->nest('content', 'products/index',array('data'=>$product_service));
    }

    public function getProductType(){
        $q = Input::get('q');
        $product_type = ProductType::where('type_name', 'like', '%'.$q.'%')->orderBy('type_name', 'asc')->limit(10)->get();
        $array = array();
        foreach($product_type as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->type_name
                );
        }
        echo json_encode($array);
    }

    public function getUpdate(ProductRequest $request){
        $id = Input::get('id');
        if($id){    
            $product_type_id = Input::get('product_type_id');
            $product_code = Input::get('product_code');
            $product_name = Input::get('product_name');
            $spesification = Input::get('spesification');
            $product_service = ProductServices::findOrFail($id);
            $product_service->product_type_id = $product_type_id;
            $product_service->product_code = $product_code;
            $product_service->product_name = $product_name;
            $product_service->spesification = $spesification;
            $product_service->save();
            return Redirect()->route('product.index');
        }
        else{
            $product_type_id = Input::get('product_type_id');
            $product_code = Input::get('product_code');
            $product_name = Input::get('product_name');
            $spesification = Input::get('spesification');  
            
            $product_service = new ProductServices;
            $product_service->product_type_id = $product_type_id;
            $product_service->product_code = $product_code;
            $product_service->product_name = $product_name;
            $product_service->spesification = $spesification;
            $product_service->save();
            return Redirect()->route('product.index');
        }
    } 
}
