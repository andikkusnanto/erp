<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\ProductType;
    use App\Http\Requests\ProductTypeRequest;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use DB;

class ProductTypeController extends Controller
{
	public function index(Request $request){
        $product_type = ProductType::all();
        return View::make('layouts/dashboard',array())->nest('content','producttypes.index',array('data'=>$product_type));
    }

    public function create(){
        $product_type = ProductType::where('id','=','-1')->get();
        return View::make('layouts/dashboard',array())->nest('content','producttypes.create',array('data'=>$product_type));
    }

    public function store(ProductTypeRequest $request){
        ProductType::create($request->all());
        return redirect()->route('producttype.index')->with('success','Item created successfully');
    }

   public function show($id){
        $product_type = ProductType::findOrFail($id);
        return View::make('layouts/dashboard',array())->nest('content','producttypes.show',array('data'=>$product_type));   
    }

    public function edit($id){
        $product_type = ProductType::findOrFail($id);
        return View::make('layouts/dashboard',array())->nest('content','producttypes.edit',array('data'=>$product_type));
    }

    public function update(ProductTypeRequest $request, $id){
        ProductType::findOrFail($id)->update($request->all());
        return redirect()->route('producttype.index')->with('success','Item updated successfully');
    }

    public function destroy($id){
        $product_type= ProductType::where('id',$id)->delete();
        return redirect()->route('producttype.index')->with('success','Item deleted successfully');
    }

    public function getSave(Request $request){
        $id = Input::get('id');
        if($id){
            $product_type = ProductType::findOrFail($id);
            $type_name = Input::get('type_name');
            $description = Input::get('description');
            $product_type->type_name = $type_name;
            $product_type->description = $description;
            $product_type->save();
            return Redirect()->route('producttype.index');
        }
        else{
            $type_name = Input::get('type_name');
            $description = Input::get('description');
            $product_type = new ProductType;
            $product_type->type_name = $type_name;
            $product_type->description = $description;
            $product_type->save();
            return Redirect()->route('producttype.index');
        }
    }

    public function getUpdate(Request $request){
        $id = Input::get('id');
        if($id){
            $product_type = ProductType::findOrFail($id);
            $type_name = Input::get('type_name');
            $description = Input::get('description');
            $product_type->type_name = $type_name;
            $product_type->description = $description;
            $product_type->save();
            return Redirect()->route('producttype.index');
        }
        else{
            $type_name = Input::get('type_name');
            $description = Input::get('description');
            $product_type = new ProductType;
            $product_type->type_name = $type_name;
            $product_type->description = $description;
            $product_type->save();
            return Redirect()->route('producttype.index');
        }
    }
}
