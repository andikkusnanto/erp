<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\ExternalProvider;
    use App\Http\Requests\ExternalProviderRequest;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use DB;

class ExternalProviderController extends Controller
{
	public function index(Request $request){
        $external_providers = ExternalProvider::all();
        return view::make('layouts/dashboard',array())->nest('content','externalproviders.index',array('data'=>$external_providers));
    }

    public function create(){
        $external_providers = ExternalProvider::where('id','=','-1')->get();
        return View::make('layouts/dashboard',array())->nest('content','externalproviders.create',array('data'=>$external_providers));
    }

    public function store(ExternalProviderRequest $request){
        ExternalProvider::create($request->all());
        return redirect()->route('externalprovider.index')->with('success','Item created successfully');
    }

    public function show($id){
       $external_provider = ExternalProvider::findOrFail($id);
       return View::make('layouts/dashboard',array())->nest('content','externalproviders.show',array('data'=>$external_provider));
    }

    public function edit($id){
        $external_provider = ExternalProvider::findOrFail($id);
        return view::make('layouts/dashboard',array())->nest('content','externalproviders.edit',array('data'=>$external_provider));
    }

    public function update(ExternalProviderRequest $request, $id){
        ExternalProvider::findOrFail($id)->update($request->all());
        return redirect()->route('externalprovider.index')->with('success','Item updated successfully');
    }

    public function destroy($id){
        $external_provider= ExternalProvider::where('id',$id)->delete();
        return redirect()->route('externalprovider.index')->with('success','Item deleted successfully');
    }
}
