<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\AppMenu;
    use App\UserRole;
    use App\UserRoleDetail;
    use App\PurchaseOrder;
    use App\DeliveryOrder;
    use App\Invoice;
    use App\PaymentVoucher;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use Session;
    use Illuminate\Support\Facades\Redirect;
    use DB;

class HomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $purchase_orders = PurchaseOrder::all();
        $total_po = DB::table('purchase_order_details')
            ->join('purchase_orders', 'purchase_order_details.purchase_order_id', '=', 'purchase_orders.id')
            ->sum('purchase_order_details.amount');

        $delivery_orders = DeliveryOrder::all();
        $total_do = DB::table('delivery_order_details')
            ->join('delivery_orders', 'delivery_order_details.delivery_order_id', '=', 'delivery_orders.id')
            ->sum('delivery_order_details.qty');

        $invoices = Invoice::all();
        $total_invoice = DB::table('invoice_details')
            ->join('invoices', 'invoice_details.invoice_id', '=', 'invoices.id')
            ->sum('invoice_details.amount');

        $payment_vouchers = PaymentVoucher::all();
        $total_pv = DB::table('payment_voucher_details')
            ->join('payment_vouchers', 'payment_voucher_details.payment_voucher_id', '=', 'payment_vouchers.id')
            ->sum('payment_voucher_details.amount');

        $financial_report=DB::select('select * from(select purchase_orders.po_number,purchase_orders.date as po_date,product_services.product_name, purchase_order_details.amount as po_amount, delivery_order_details.do_number, delivery_orders.date as do_date,delivery_order_details.remark as do_remark, invoice_details.invoice_id,invoice_details.invoice_number, invoices.date as invoice_date, invoice_details.amount as invoice_amount, payment_voucher_details.ID as pv_id,payment_voucher_details.pv_number, payment_voucher_details.amount as pv_amount,payment_voucher_details.remark as pv_remark, payment_vouchers.date as pv_date
            FROM (((((((purchase_orders LEFT JOIN purchase_order_details ON purchase_orders.ID = purchase_order_details.purchase_order_id) LEFT JOIN delivery_order_details ON purchase_order_details.ID = delivery_order_details.purchase_order_detail_id) LEFT JOIN invoice_details ON delivery_order_details.ID = invoice_details.delivery_order_detail_id) LEFT JOIN payment_voucher_details ON invoice_details.invoice_id = payment_voucher_details.invoice_id) LEFT JOIN payment_vouchers ON payment_voucher_details.payment_voucher_id = payment_vouchers.ID) LEFT JOIN invoices ON invoice_details.invoice_id = invoices.ID) LEFT JOIN delivery_orders ON delivery_order_details.delivery_order_id = delivery_orders.ID) LEFT JOIN product_services ON purchase_order_details.product_service_id = product_services.ID)TBL01 Order By po_number asc');

        $options = array(
            'purchase_orders'=>$purchase_orders,
            'total_po'=>$total_po,
            'delivery_orders'=>$delivery_orders,
            'total_do'=>$total_do,
            'invoices'=>$invoices,
            'total_invoice'=>$total_invoice,
            'payment_vouchers'=>$payment_vouchers,
            'total_pv'=>$total_pv,
            'financial_report'=>$financial_report,
            );

        return View::make('layouts/dashboard',array())->nest('content', '/home',$options);
    }
}
