<?php

    namespace App\Http\Controllers;
    use DB;
    use Session;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Redirect;
    use App\Http\Requests\ProductRequest;
    use App\Http\Controllers\Controller;
    use App\DeliveryOrder;
    use App\DeliveryOrderDetail;
    use App\ExternalProvider;
    use App\ProductServices;
    use App\PurchaseOrderDetail;
    use App\Http\Requests\DeliveryOrderRequest;
    use App\Uom;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;

class DeliveryOrderController extends Controller{
    public function index(Request $request){
        $first_date = Input::get('date_first');
        $last_date = Input::get('date_last');
        if(isset($first_date) && isset($last_date)){
            $delivery_order = DeliveryOrder::where('date','>=',$first_date)->where('date','<=',$last_date)->orderBy('external_provider_id', 'asc')->orderBy('date', 'asc')->paginate(10);
        }else{
            $delivery_order = DeliveryOrder::orderBy('external_provider_id', 'asc')->orderBy('date', 'asc')->paginate(10);
        }
        return View::make('layouts/dashboard',array())->nest('content', 'deliveryorders.index',array('data'=>$delivery_order));
    }

    public function create(){
        $delivery_orders = DeliveryOrder::all();
        $external_providers = ExternalProvider::all();
        $options = array(
            'data'=>$delivery_orders,
            'externalprovider'=>$external_providers,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'deliveryorders.create',$options);
    }

    public function store(ProductRequest $request){
        DeliveryOrder::create($request->all());
        return redirect()->route('deliveryorder.index')->with('success','Item created successfully');
    }

    public function show(Request $request, $id){
        $delivery_order = DeliveryOrder::findOrFail($id);
        return View::make('layouts/dashboard',array())->nest('content', 'deliveryorders.show',array('delivery_order'=>$delivery_order));
    }

    public function edit(Request $request, $id){
        $delivery_order = DeliveryOrder::findOrFail($id);
        $delivery_order_detail = DeliveryOrderDetail::where('delivery_order_id','=',$id)->get();
        $external_providers = ExternalProvider::all();
        $purchase_order_details= PurchaseOrderDetail::all();
        $uoms= Uom::all();
        $options = array(
            'delivery_order'=>$delivery_order,
            'delivery_order_detail'=>$delivery_order_detail,
            'external_providers'=>$external_providers,
            'purchase_order_details'=>$purchase_order_details,
            'uoms'=>$uoms,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'deliveryorders.edit',$options);
     }

    public function update(DeliveryOrderRequest $request, $id){
        DeliveryOrder::findOrFail($id)->update($request->all());
        return redirect()->route('deliveryorder.index')->with('success','Item updated successfully');
    }

    public function destroy($id){
        try{
            $delivery_order_detail = DeliveryOrderDetail::where('delivery_order_id',$id)->delete();
            $delivery_order= DeliveryOrder::where('id',$id)->delete();
            // Session::flash('flash_warning', 'Data has been deleted.');
            return redirect()->route('deliveryorder.index');
        }
        catch(\Exception $e){
            return redirect()->route('deliveryorder.index')->with('flash_warning','Data has been deleted.');
        }
    }

    public function search(Request $request){
        $query = $request->get('search');
        $purchase_order = PurchaseOrder::where('product_name', 'LIKE', '%' . $query . '%')->orWhere('spesification', 'LIKE', '%' . $query . '%')->orWhere('product_code', 'LIKE', '%' . $query . '%')->paginate(5);
        
        return view('products.index',compact('purchase_order'))->with('b', ($request->input('page', 1) - 1) * 5);
    }

    public function getExternalProvider(){
        $q = Input::get('q');
        $external_provider = ExternalProvider::where('expname', 'like', '%'.$q.'%')->orderBy('expname', 'asc')->limit(10)->get();
        $array = array();
        foreach($external_provider as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->expname
                );
        }
        echo json_encode($array);
    }

    public function getPurchaseOrder(){
        $q = Input::get('q');
        $purchase_order_detail = PurchaseOrderDetail::where('po_number', 'like', '%'.$q.'%')->orderBy('po_number', 'asc')->limit(10)->get();
        $array = array();
        foreach($purchase_order_detail as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->po_number,
                'text1'=> $row->productservice->product_name . ', ' . $row->qty . ' ' . $row->uom->name         
                );
        }
        echo json_encode($array);
    }

    public function getUom(Request $request){
        $purchase_order_detail = PurchaseOrderDetail::Select('uom_id')->where('id',$request->id)->first();
        return response()->json($purchase_order_detail);
    }

    public function getUomName(Request $request){
        $uom = Uom::Select('name')->where('id',$request->id)->first();
        return response()->json($uom);
    }

    public function getsave(DeliveryOrderRequest $request){
        $id = Input::get('id');
        DB::beginTransaction();
        try {
            if($id){
                $do_number = Input::get('do_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }
                $ship_to = Input::get('ship_to');
                $idx=Input::get('counterx');
                $receiver = Input::get('receiver');
                $approval = Input::get('approval');
                $remark = Input::get('remark');
                
                $DeliveryOrder = DeliveryOrder::findOrFail($id);
                $DeliveryOrder->do_number = $do_number;
                $DeliveryOrder->date = $date;
                $DeliveryOrder->external_provider_id = $external_provider_id;
                $DeliveryOrder->ship_to = $ship_to;
                $DeliveryOrder->receiver = $receiver;
                $DeliveryOrder->approval = $approval;
                $DeliveryOrder->remark = $remark;
                $DeliveryOrder->save();

                for($i=0;$i<=$idx-1;$i++){
                    $delivery_order_id = $DeliveryOrder->id;
                    $purchase_order_detail_id = Input::get('purchase_order_detil_id'.$i);
                    $purchase_order_detail=PurchaseOrderDetail::find($purchase_order_detail_id);
                    $po_number=$purchase_order_detail->po_number;
                    $product_code=$purchase_order_detail->product_code;
                    $product_service_id=$purchase_order_detail->product_service_id;
                    $uom_id = $purchase_order_detail->uom_id;
                    $qty= Input::get('qty'.$i);
                    $remark_detail= Input::get('remark'.$i);

                    $check_record = DeliveryOrderDetail::where('delivery_order_id','=',$delivery_order_id)->where('purchase_order_detail_id','=',$purchase_order_detail_id)->first();

                    if(empty($check_record)){
                        
                        $DeliveryOrderDetail = new DeliveryOrderDetail;
                        $DeliveryOrderDetail->delivery_order_id = $delivery_order_id;
                        $DeliveryOrderDetail->purchase_order_detail_id = $purchase_order_detail_id;
                        $DeliveryOrderDetail->product_service_id = $product_service_id;
                        $DeliveryOrderDetail->do_number=$do_number;
                        $DeliveryOrderDetail->po_number=$po_number;
                        $DeliveryOrderDetail->product_code=$product_code;
                        $DeliveryOrderDetail->qty = $qty;
                        $DeliveryOrderDetail->uom_id = $uom_id;
                        $DeliveryOrderDetail->remark = $remark_detail;
                        $DeliveryOrderDetail->save();
                    }else{
                        $DeliveryOrderDetail = DeliveryOrderDetail::where('delivery_order_id','=',$delivery_order_id)->where('purchase_order_detail_id','=',$purchase_order_detail_id)->first();
                                           
                        $DeliveryOrderDetail->delivery_order_id = $delivery_order_id;
                        $DeliveryOrderDetail->purchase_order_detail_id = $purchase_order_detail_id;
                        $DeliveryOrderDetail->product_service_id = $product_service_id;
                        $DeliveryOrderDetail->do_number=$do_number;
                        $DeliveryOrderDetail->po_number=$po_number;
                        $DeliveryOrderDetail->product_code=$product_code;
                        $DeliveryOrderDetail->qty = $qty;
                        $DeliveryOrderDetail->uom_id = $uom_id;
                        $DeliveryOrderDetail->remark = $remark_detail;
                        $DeliveryOrderDetail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('deliveryorder.index');
            }else{
                $do_number = Input::get('do_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }

                $ship_to = Input::get('ship_to');
                $idx=Input::get('counterx');
                $receiver = Input::get('receiver');
                $approval = Input::get('approval');
                $remark = Input::get('remark');

                $DeliveryOrder = new DeliveryOrder;
                $DeliveryOrder->do_number = $do_number;
                $DeliveryOrder->date = $date;
                $DeliveryOrder->external_provider_id = $external_provider_id;
                $DeliveryOrder->ship_to = $ship_to;
                $DeliveryOrder->receiver = $receiver;
                $DeliveryOrder->approval = $approval;
                $DeliveryOrder->remark = $remark;
                $DeliveryOrder->save();

                for($j=0;$j<=$idx;$j++){
                    $delivery_order_id = $DeliveryOrder->id;
                    $purchase_order_detail_id = Input::get('purchase_order_detil_id'.$j);
                    $purchase_order_detail=PurchaseOrderDetail::find($purchase_order_detail_id);
                    $po_number=$purchase_order_detail->po_number;
                    $product_code=$purchase_order_detail->product_code;
                    $product_service_id=$purchase_order_detail->product_service_id;
                    $uom_id = $purchase_order_detail->uom_id;
                    $qty= Input::get('qty'.$j);
                    $remark_detail= Input::get('remark'.$j);

                    $check_record = DeliveryOrderDetail::where('delivery_order_id','=',$delivery_order_id)->where('purchase_order_detail_id','=',$purchase_order_detail_id)->first();

                    if(empty($check_record)){
                        $DeliveryOrderDetail = new DeliveryOrderDetail;
                        $DeliveryOrderDetail->delivery_order_id = $delivery_order_id;
                        $DeliveryOrderDetail->purchase_order_detail_id = $purchase_order_detail_id;
                        $DeliveryOrderDetail->product_service_id = $product_service_id;
                        $DeliveryOrderDetail->do_number=$do_number;
                        $DeliveryOrderDetail->po_number=$po_number;
                        $DeliveryOrderDetail->product_code=$product_code;
                        $DeliveryOrderDetail->qty = $qty;
                        $DeliveryOrderDetail->uom_id = $uom_id;
                        $DeliveryOrderDetail->remark = $remark_detail;
                        $DeliveryOrderDetail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('deliveryorder.index');
            }
        }
        catch (\Exception $e){
            DB::rollback();
            return back()->withInput()->withErrors('Data is invalid');
        }
    }
}