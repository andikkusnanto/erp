<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Redirect;
    use App\Http\Requests\InvoiceRequest;
    use App\Http\Controllers\Controller;
    use App\Invoice;
    use App\InvoiceDetail;
    use App\ExternalProvider;
    use App\ProductServices;
    use App\DeliveryOrderDetail;
    use App\PurchaseOrderDetail;
    use App\Uom;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use DB;
    use Session;

class InvoiceController extends Controller{
    
    public function index(Request $request){
        $first_date = Input::get('date_first');
        $last_date = Input::get('date_last');

        if(isset($first_date) && isset($last_date)){

            $invoice = Invoice::where('date','>=',$first_date)->where('date','<=',$last_date)->orderBy('date', 'asc')->orderBy('invoice_number', 'asc')->paginate(10);
        }else{

            $invoice = Invoice::orderBy('date', 'asc')->orderBy('invoice_number', 'asc')->paginate(10);
        }
        return View::make('layouts/dashboard',array())->nest('content', 'invoices.index',array('data'=>$invoice));
    }

    public function totals(Request $request){
        $invoice_detail_amount = DB::table('invoice_details')
            ->where('invoice_id', '=', $request->id)
            ->sum('amount');
        return response()->json($invoice_detail_amount);
    }

    public function create(){
        $invoices = Invoice::all();
        $external_providers = ExternalProvider::all();

        $options = array(
            'invoices'=>$invoices,
            'external_providers'=>$external_providers,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'invoices.create',$options);
    }

    public function show(Request $request, $id){
        $invoice = Invoice::findOrFail($id);
        $invoice_detail_amount = DB::table('invoice_details')
            ->join('invoices', 'invoice_details.invoice_id', '=', 'invoices.id')
            ->where('invoices.id', '=', $id)
            ->sum('invoice_details.amount');

        $options = array(
            'invoice'=>$invoice,
            'invoice_detail_amount'=>$invoice_detail_amount,
            );

        return View::make('layouts/dashboard',array())->nest('content', 'invoices.show',$options);
    }

    public function edit(Request $request, $id){
        $invoice = Invoice::findOrFail($id);
        $invoice_detail = InvoiceDetail::where('Invoice_id','=',$id)->get();
        $external_providers = ExternalProvider::all();
        $delivery_order_details = DeliveryOrderDetail::all();
        $uoms= Uom::all();

        $options = array(
            'invoice'=>$invoice,
            'invoice_detail'=>$invoice_detail,
            'external_providers'=>$external_providers,
            'delivery_order_details'=>$delivery_order_details,
            'uoms'=>$uoms,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'invoices.edit',$options);
     }
    
    public function destroy($id){
        try{
            $invoice_detail = InvoiceDetail::where('invoice_id',$id)->delete();
            $invoice= Invoice::where('id',$id)->delete();
            // Session::flash('flash_warning', 'Data has been deleted.');
            return redirect()->route('invoice.index');
        }
        catch(\Exception $e){
            return redirect()->route('invoice.index')->with('flash_warning','Data has been deleted.');
        }
    }

    public function getExternalProvider(){
        $q = Input::get('q');
        $external_provider = ExternalProvider::where('expname', 'like', '%'.$q.'%')->orderBy('expname', 'asc')->limit(10)->get();
        $array = array();
        foreach($external_provider as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->expname
                );
        }
        echo json_encode($array);
    }

    public function getDeliveryOrder(){
        $q = Input::get('q');
        $delivery_order_detail = DeliveryOrderDetail::where('do_number', 'like', '%'.$q.'%')->orderBy('do_number', 'asc')->limit(10)->get();

        $array = array();
        foreach($delivery_order_detail as $row){
            
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->do_number,
                'text1'=>$row->productService->product_name . ', ' . $row->qty .' ' . $row->uom->name         
                );
        }

        echo json_encode($array);
    }

    public function getUom(){
        $q = Input::get('q');
        $uom = Uom::where('name', 'like', '%'.$q.'%')->orderBy('name', 'asc')->limit(10)->get();
        $array = array();
        foreach($uom as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->name
                );
        }
        echo json_encode($array);
    }

    public function getsave(InvoiceRequest $request){
        $id = Input::get('id');
        DB::beginTransaction();
        try {
            if($id){
                $invoice_number = Input::get('invoice_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }
                $remark = Input::get('remark');
                $idx=Input::get('counterx');

                $invoice = Invoice::findOrFail($id);
                $invoice->invoice_number = $invoice_number;
                $invoice->date = $date;
                $invoice->external_provider_id = $external_provider_id;
                $invoice->remark = $remark;
                $invoice->save();

                for($i=0;$i<=$idx-1;$i++){
                    $delivery_order_detail_id = Input::get('delivery_order_detil_id'.$i);
                    $invoice_id = $invoice->id;
                    $delivery_order_detail = DeliveryOrderDetail::find($delivery_order_detail_id);
                    $purchase_order_detail = PurchaseOrderDetail::find($delivery_order_detail->purchase_order_detail_id);
                    $qty = $delivery_order_detail->qty;
                    $uom_id = $delivery_order_detail->uom_id;
                    $unit_price = $purchase_order_detail->unit_price;
                    $amount=$qty*$unit_price;
                    $check_record = InvoiceDetail::where('invoice_id','=',$invoice->id)->where('delivery_order_detail_id','=',$delivery_order_detail_id)->first();
                    if(empty($check_record)){
                        $invoice_detail = new InvoiceDetail;
                        $invoice_detail->invoice_id = $invoice->id;
                        $invoice_detail->delivery_order_detail_id = $delivery_order_detail_id;
                        $invoice_detail->invoice_number = $invoice_number;
                        $invoice_detail->po_number = $delivery_order_detail->po_number;
                        $invoice_detail->do_number = $delivery_order_detail->do_number;
                        $product_service = ProductServices::find($delivery_order_detail->product_service_id);
                        $invoice_detail->product_code = $product_service->product_code;
                        $invoice_detail->qty =  $qty;
                        $invoice_detail->uom_id =$uom_id;  
                        $invoice_detail->unit_price =$unit_price; 
                        $invoice_detail->amount =$amount;
                        $invoice_detail->remark =Input::get('remark'.$i); 
                        $invoice_detail->save();
                    }else{
                        $invoice_detail = InvoiceDetail::where('invoice_id','=',$invoice->id)->where('delivery_order_detail_id','=',$delivery_order_detail_id)->first();
                        $invoice_detail->invoice_id = $invoice->id;
                        $invoice_detail->delivery_order_detail_id = $delivery_order_detail_id;
                        $invoice_detail->invoice_number = $invoice_number;
                        $invoice_detail->po_number = $delivery_order_detail->po_number;
                        $invoice_detail->do_number = $delivery_order_detail->do_number;
                        $product_service = ProductServices::find($delivery_order_detail->product_service_id);
                        $invoice_detail->product_code = $product_service->product_code;
                        $invoice_detail->qty =  $qty;
                        $invoice_detail->uom_id =$uom_id;  
                        $invoice_detail->unit_price =$unit_price; 
                        $invoice_detail->amount =$amount;
                        $invoice_detail->remark =Input::get('remark'.$i); 
                        $invoice_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('invoice.index');
            }else{
                $invoice_number = Input::get('invoice_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }
                $remark = Input::get('remark');
                $idx=Input::get('counterx');
                $invoice = new Invoice;
                $invoice->invoice_number = $invoice_number;
                $invoice->date = $date;
                $invoice->external_provider_id = $external_provider_id;
                $invoice->remark = $remark;
                $invoice->save();

                for($j=0;$j<=$idx;$j++){
                    $delivery_order_detail_id = Input::get('delivery_order_detil_id'.$j);
                    $invoice_id = $invoice->id;
                    $delivery_order_detail=DeliveryOrderDetail::find($delivery_order_detail_id);
                    $purchase_order_detail=PurchaseOrderDetail::find($delivery_order_detail->purchase_order_detail_id);
                    $qty=$delivery_order_detail->qty;
                    $uom_id=$delivery_order_detail->uom_id;
                    $unit_price=$purchase_order_detail->unit_price;
                    $amount=$qty*$unit_price;

                    $check_record = InvoiceDetail::where('invoice_id','=',$invoice->id)->where('delivery_order_detail_id','=',$delivery_order_detail_id)->first();
                    if(empty($check_record)){
                        $invoice_detail = new InvoiceDetail;
                        $invoice_detail->invoice_id = $invoice->id;
                        $invoice_detail->delivery_order_detail_id = $delivery_order_detail_id;
                        $invoice_detail->invoice_number = $invoice_number;
                        $invoice_detail->po_number = $delivery_order_detail->po_number;
                        $invoice_detail->do_number = $delivery_order_detail->do_number;
                        $product_service = ProductServices::find($delivery_order_detail->product_service_id);
                        $invoice_detail->product_code = $product_service->product_code;
                        $invoice_detail->qty =  $qty;
                        $invoice_detail->uom_id =$uom_id;  
                        $invoice_detail->unit_price =$unit_price; 
                        $invoice_detail->amount =$amount;
                        $invoice_detail->remark =Input::get('remark'.$j); 
                        $invoice_detail->save();
                    }else{
                        $invoice_detail = InvoiceDetail::where('invoice_id','=',$invoice->id)->where('delivery_order_detail_id','=',$delivery_order_detail_id)->first();
                        $invoice_detail->invoice_id = $invoice->id;
                        $invoice_detail->delivery_order_detail_id = $delivery_order_detail_id;
                        $invoice_detail->invoice_number = $invoice_number;
                        $invoice_detail->po_number = $delivery_order_detail->po_number;
                        $invoice_detail->do_number = $delivery_order_detail->do_number;
                        $product_service = ProductServices::find($delivery_order_detail->product_service_id);
                        $invoice_detail->product_code = $product_service->product_code;
                        $invoice_detail->qty =  $qty;
                        $invoice_detail->uom_id =$uom_id;  
                        $invoice_detail->unit_price =$unit_price; 
                        $invoice_detail->amount =$amount;
                        $invoice_detail->remark =Input::get('remark'.$j); 
                        $invoice_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('invoice.index');
            }
        }
        catch (\Exception $e){
            DB::rollback();
            return back()->withInput()->withErrors('Data is invalid');
        }
    }
}
