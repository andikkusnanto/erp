<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\User;
    use App\UserRole;
    use App\UserRoleDetail;
    use App\AppMenu;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use Session;
    use Illuminate\Support\Facades\Redirect;
    use DB;

class UserRoleController extends Controller
{
     public function index(Request $request){
        $user_role =UserRole::All();

        return view::make('layouts/dashboard',array())->nest('content', 'userroles.index',array('user_role'=>$user_role));
    }

    public function create(){
        $user_role = UserRole::all();
        $options = array(
            'user_role'=>$user_role,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'userroles.create',$options);
   }

    public function store(ProductRequest $request){
        PurchaseOrder::create($request->all());
        return redirect()->route('purchaseorders.index')->with('success','Item created successfully');
    }

    public function show(Request $request, $id){
        $user_role = UserRole::findOrFail($id);
        $user_role_detail = UserRoleDetail::where('user_role_id','=',$id)->get();
        $app_menus = AppMenu::all();

        $options = array(
            'user_role'=>$user_role,
            'user_role_detail'=>$user_role_detail,
            'app_menus'=>$app_menus,
        );
        return View::make('layouts/dashboard',array())->nest('content', 'userroles.show',$options);
    }

    public function edit(Request $request, $id){
        $user_role = UserRole::findOrFail($id);
        $user_role_detail = UserRoleDetail::where('user_role_id','=',$id)->get();
        $app_menus = AppMenu::all();

        $options = array(
            'user_role'=>$user_role,
            'user_role_detail'=>$user_role_detail,
            'app_menus'=>$app_menus,
        );
        return View::make('layouts/dashboard',array())->nest('content', 'userroles.edit',$options);
    }

    public function update(ProductRequest $request, $id){
        PurchaseOrder::findOrFail($id)->update($request->all());
        return redirect()->route('product.index')->with('success','Item updated successfully');
    }

    public function destroy($id){
        try{
            $user_role_detil = UserRoleDetil::where('user_role_id',$id)->delete();
            $User_role= UserRole::where('id',$id)->delete();
            Session::flash('flash_message', 'Data has been deleted.');
            return redirect()->route('userrole.index');
        }                   
        catch(\Exception $e){
            return redirect()->route('userrole.index')->with('flash_warning','Data has been deleted.');
        }
    }

    public function getAppMenu(){
        $q = Input::get('q');
        $app_menu = AppMenu::where('name', 'like', '%'.$q.'%')->orderBy('name', 'asc')->limit(10)->get();

        $array = array();
        foreach($app_menu as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->name
                );
        }
        echo json_encode($array);
    }

    public function getSave(Request $request){
        $id = Input::get('id');
        DB::beginTransaction();
        try {
            if($id){
                $role = Input::get('role');
                $description = Input::get('description');
                $idx=Input::get('counterx');
                $user_role = UserRole::findOrFail($id);
                $user_role->role = $role;
                $user_role->description = $description;
                $user_role->save();

                for($i=0;$i<=$idx-1;$i++){
                    $app_menu_id = Input::get('app_menu_id'.$i);
                    $user_role_id = $user_role->id;
                    $check_record = UserRoleDetail::where('user_role_id','=',$user_role->id)->where('app_menu_id','=',$app_menu_id)->first();

                    if(empty($check_record)){
                        $user_role_detail = new UserRoleDetail;
                        $user_role_detail->user_role_id = $user_role->id;
                        $user_role_detail->app_menu_id = $app_menu_id;
                        $user_role_detail->privileges =Input::get('privileges'.$i); 
                        $user_role_detail->description =Input::get('description'.$i);
                        $user_role_detail->save();
                    }else{
                        $user_role_detail = UserRoleDetail::where('user_role_id','=',$user_role->id)->where('app_menu_id','=',$app_menu_id)->first();
                        $user_role_detail->user_role_id = $user_role->id;
                        $user_role_detail->app_menu_id = $app_menu_id;
                        $user_role_detail->privileges =Input::get('privileges'.$i); 
                        $user_role_detail->description =Input::get('description'.$i);
                        $user_role_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('userrole.index');
            }else{
                $role = Input::get('role');
                $description = Input::get('description');
                $idx=Input::get('counterx');
                $user_role = new UserRole;
                $user_role->role = $role;
                $user_role->description = $description;
                $user_role->save();

                for($j=0;$j<=$idx;$j++){
                    $app_menu_id = Input::get('app_menu_id'.$j);
                    $check_record = UserRoleDetail::where('user_role_id','=',$user_role->id)->where('app_menu_id','=',$app_menu_id)->first();
                    if(empty($check_record)){
                        $user_role_detail = new UserRoleDetail;
                        $user_role_detail->user_role_id = $user_role->id;
                        $user_role_detail->app_menu_id = $app_menu_id;
                        $user_role_detail->privileges =Input::get('privileges'.$j); 
                        $user_role_detail->description =Input::get('description'.$j);
                        $user_role_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('userrole.index');
            }
        }
        catch (\Exception $e){
            DB::rollback();
            return back()->withInput()->withErrors('Data is invalid');
        }
    }
}
