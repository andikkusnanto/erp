<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Uom;
    use App\Http\Requests\UomRequest;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use DB;

class UomController extends Controller{
    public function index(Request $request){
        $uoms = Uom::orderBy('name','ASC')->get();
        return view::make('layouts/dashboard',array())->nest('content', 'uoms.index',array('uoms'=>$uoms));
    }

    public function create(){
        $uom = Uom::where('id','=','-1')->get();
        return view::make('layouts/dashboard',array())->nest('content', 'uoms.create',array('uom'=>$uom));
    }

    public function store(UomRequest $request){
        Uom::create($request->all());
        return redirect()->route('uom.index')->with('success','Item created successfully');
    }

    public function show($id){
        $uom = Uom::findOrFail($id);
        return view::make('layouts/dashboard',array())->nest('content', 'uoms.show',array('uom'=>$uom));
    }

    public function edit($id){
        $uom = Uom::findOrFail($id);
        return view::make('layouts/dashboard',array())->nest('content', 'uoms.edit',array('uom'=>$uom));
    }

    public function update(UomRequest $request, $id){
        Uom::findOrFail($id)->update($request->all());
        return redirect()->route('uom.index')->with('success','Item updated successfully');
    }

    public function destroy($id){
         $uom = Uom::findOrFail($id)->delete();
        return redirect()->route('uom.index')->with('success','Item deleted successfully');
    }
}	