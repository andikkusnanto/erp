<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Redirect;
    use App\Http\Requests\InvoiceRequest;
    use App\Http\Requests\PaymentVoucherRequest;
    use App\Http\Controllers\Controller;
    use App\Invoice;
    use App\InvoiceDetail;
    use App\ExternalProvider;
    use App\ProductServices;
    use App\DeliveryOrderDetail;
    use App\PurchaseOrderDetail;
    use App\Uom;
    use App\PaymentVoucher;
    use App\PaymentVoucherDetail;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use DB;

class PaymentVoucherController extends Controller
{
     public function index(Request $request){
        $first_date = Input::get('date_first');
        $last_date = Input::get('date_last');
        if(isset($first_date) && isset($last_date)){
            $payment_voucher = PaymentVoucher::where('date','>=',$first_date)->where('date','<=',$last_date)->orderBy('date', 'asc')->orderBy('pv_number', 'asc')->paginate(10);
        }else{
            $payment_voucher = PaymentVoucher::orderBy('date', 'asc')->orderBy('pv_number', 'asc')->paginate(10);
        }
        return view::make('layouts/dashboard',array())->nest('content', 'paymentvouchers.index',array('data'=>$payment_voucher));
    }

    public function totals(Request $request){
        $payment_voucher_detail_amount = DB::table('payment_voucher_details')
            ->where('payment_voucher_id', '=', $request->id)
            ->sum('amount');
        return response()->json($payment_voucher_detail_amount);
    }

    public function create(){
        $invoices = Invoice::all();
        $external_providers = ExternalProvider::all();
        $options = array(
            'invoices'=>$invoices,
            'external_providers'=>$external_providers,
            );
        return view::make('layouts/dashboard',array())->nest('content', 'paymentvouchers.create',$options);
    }

    public function show(Request $request, $id){
        $payment_voucher = PaymentVoucher::findOrFail($id);
        $payment_voucher_detail = PaymentVoucherDetail::where('payment_voucher_id','=',$id)->get();
        $external_providers = ExternalProvider::all();
        $invoices= Invoice::all();
        $uoms= Uom::all();
        
        $payment_voucher_detail_amount = DB::table('payment_voucher_details')
            ->join('payment_vouchers', 'payment_voucher_details.payment_voucher_id', '=', 'payment_vouchers.id')
            ->where('payment_vouchers.id', '=', $id)
            ->sum('payment_voucher_details.amount');

       $options = array(
            'payment_voucher'=>$payment_voucher,
            'payment_voucher_detail'=>$payment_voucher_detail,
            'external_providers'=>$external_providers,
            'invoices'=>$invoices,
            'uoms'=>$uoms,
            'payment_voucher_detail_amount'=>$payment_voucher_detail_amount,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'paymentvouchers.show',$options);
    }

    public function edit(Request $request, $id){
        $payment_voucher = PaymentVoucher::findOrFail($id);
        $payment_voucher_detail = PaymentVoucherDetail::where('payment_voucher_id','=',$id)->get();
        $external_providers = ExternalProvider::all();
        $invoices= Invoice::all();
        $uoms= Uom::all();

        $options = array(
            'payment_voucher'=>$payment_voucher,
            'payment_voucher_detail'=>$payment_voucher_detail,
            'external_providers'=>$external_providers,
            'invoices'=>$invoices,
            'uoms'=>$uoms,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'paymentvouchers.edit',$options);
     }

    public function destroy($id){
        try{
            $payment_voucher_detail = PaymentVoucherDetail::where('payment_voucher_id',$id)->delete();
            $payment_voucher= PaymentVoucher::where('id',$id)->delete();
            // Session::flash('flash_warning', 'Data has been deleted.');
            return redirect()->route('paymentvoucher.index');
        }
        catch(\Exception $e){
            return redirect()->route('invoice.index')->with('flash_warning','Data has been deleted.');
        }
    }

    public function getExternalProvider(){
        $q = Input::get('q');
        $external_provider = ExternalProvider::where('expname', 'like', '%'.$q.'%')->orderBy('expname', 'asc')->limit(10)->get();
        $array = array();
        foreach($external_provider as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->expname
                );
        }
        echo json_encode($array);
    }

    public function getInvoice(){
        $q = Input::get('q');
        $invoice = Invoice::where('invoice_number', 'like', '%'.$q.'%')->orderBy('invoice_number', 'asc')->limit(10)->get();

        $array = array();
        foreach($invoice as $row){
            $invoice_detail_amount = DB::table('invoice_details')->where('invoice_id', '=', $row->id)->sum('amount');

            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->invoice_number,
                'text1'=>$row->date . ', Amount = ' . number_format($invoice_detail_amount,0,",",".")
            );
        }

        echo json_encode($array);
    }

    public function getUom(){
        $q = Input::get('q');
        $uom = Uom::where('name', 'like', '%'.$q.'%')->orderBy('name', 'asc')->limit(10)->get();
        $array = array();
        foreach($uom as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->name
                );
        }
        echo json_encode($array);
    }

    public function getSave(PaymentVoucherRequest $request){
        $id = Input::get('id');
        DB::beginTransaction();
        try {
            if($id){
                $pv_number = Input::get('pv_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }
                $remark = Input::get('remark');
                $idx=Input::get('counterx');

                $payment_voucher = PaymentVoucher::findOrFail($id);
                $payment_voucher->pv_number = $pv_number;
                $payment_voucher->date = $date;
                $payment_voucher->external_provider_id = $external_provider_id;
                $payment_voucher->status = "Payed";
                $payment_voucher->remark = $remark;
                $payment_voucher->save();

                for($i=0;$i<=$idx-1;$i++){
                    $payment_voucher_id = $payment_voucher->id;
                    $invoice_id = Input::get('invoice_id'.$i);
                    $invoice_detail_amount = DB::table('invoice_details')->where('invoice_id', '=', $invoice_id)->sum('amount');
                    $check_record = PaymentVoucherDetail::where('payment_voucher_id','=',$payment_voucher_id)->where('invoice_id','=',$invoice_id)->first();
                    if(empty($check_record)){
                        $payment_voucher_detail = new PaymentVoucherDetail;
                        $payment_voucher_detail->payment_voucher_id = $payment_voucher_id;
                        $payment_voucher_detail->invoice_id = $invoice_id;
                        $payment_voucher_detail->pv_number = $pv_number;
                        $payment_voucher_detail->amount = $invoice_detail_amount;
                        $payment_voucher_detail->remark =Input::get('remark'.$i); 
                        $payment_voucher_detail->save();
                    }else{
                        $payment_voucher_detail = PaymentVoucherDetail::where('payment_voucher_id','=',$payment_voucher_id)->where('invoice_id','=',$invoice_id)->first();
                        $payment_voucher_detail->payment_voucher_id = $payment_voucher_id;
                        $payment_voucher_detail->invoice_id = $invoice_id;
                        $payment_voucher_detail->pv_number = $pv_number;
                        $payment_voucher_detail->amount = $invoice_detail_amount;
                        $payment_voucher_detail->remark =Input::get('remark'.$i);  
                        $payment_voucher_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('paymentvoucher.index');
            }else{
                $pv_number = Input::get('pv_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }
                $remark = Input::get('remark');
                $idx=Input::get('counterx');
                $payment_voucher = new PaymentVoucher;
                $payment_voucher->pv_number = $pv_number;
                $payment_voucher->date = $date;
                $payment_voucher->external_provider_id = $external_provider_id;
                $payment_voucher->status = "Payed";
                $payment_voucher->remark = $remark;
                $payment_voucher->save();

                for($j=0;$j<=$idx;$j++){
                    $payment_voucher_id = $payment_voucher->id;
                    $invoice_id = Input::get('invoice_id'.$j);
                    $invoice_detail_amount = DB::table('invoice_details')->where('invoice_id', '=', $invoice_id)->sum('amount');
                    $check_record = PaymentVoucherDetail::where('payment_voucher_id','=',$payment_voucher_id)->where('invoice_id','=',$invoice_id)->first();
                    if(empty($check_record)){
                        $payment_voucher_detail = new PaymentVoucherDetail;
                        $payment_voucher_detail->payment_voucher_id = $payment_voucher_id;
                        $payment_voucher_detail->invoice_id = $invoice_id;
                        $payment_voucher_detail->pv_number = $pv_number;
                        $payment_voucher_detail->amount = $invoice_detail_amount;
                        $payment_voucher_detail->remark =Input::get('remark'.$j); 
                        $payment_voucher_detail->save();
                    }else{
                        $payment_voucher_detail = PaymentVoucherDetail::where('payment_voucher_id','=',$payment_voucher_id)->where('invoice_id','=',$invoice_id)->first();
                        $payment_voucher_detail->payment_voucher_id = $payment_voucher_id;
                        $payment_voucher_detail->invoice_id = $invoice_id;
                        $payment_voucher_detail->pv_number = $pv_number;
                        $payment_voucher_detail->amount = $invoice_detail_amount;
                        $payment_voucher_detail->remark =Input::get('remark'.$j);  
                        $payment_voucher_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('paymentvoucher.index');
            }
        }
        catch (\Exception $e){
            DB::rollback();
            return back()->withInput()->withErrors('Data is invalid');
        }
    }
}
