<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Http\Requests\AppMenuRequest;
    use App\Http\Controllers\Controller;
    use App\AppMenu;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use DB;

class AppMenuController extends Controller
{
    public function index(Request $request){
        $app_menu = AppMenu::all();
        return View::make('layouts/dashboard',array())->nest('content', 'appmenus/index',array('data'=>$app_menu));
    }

    public function create(Request $request){
        $app_menu = AppMenu::all();
        $options = array(
            'data'=>$app_menu,
            );

        return View::make('layouts/dashboard',array())->nest('content', 'appmenus.create',$options);
    }

    public function show($id){
        $app_menu = AppMenu::findOrFail($id);
        return View::make('layouts/dashboard',array())->nest('content', 'appmenus.show',array('data'=>$app_menu));
    }                                                                                                       

    public function edit(Request $request, $id){
        $app_menu = AppMenu::findOrFail($id);
        $options = array(
            'data'=>$app_menu,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'appmenus.edit',$options);
    }

    public function destroy($id){
        $app_menu = AppMenu::findOrFail($id)->delete();
        return redirect()->route('appmenu.index')->with('success','Item deleted successfully');
    }

    public function getproducttype(){
        $q = Input::get('q');
        $product_type = ProductType::where('type_name', 'like', '%'.$q.'%')->orderBy('type_name', 'asc')->limit(10)->get();
        $array = array();
        foreach($product_type as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->type_name
                );
        }
        echo json_encode($array);
    }

    public function getsave(AppMenuRequest $request){
        $id = Input::get('id');
        if($id){    
            $app_menu = AppMenu::findOrFail($id);
            $app_menu->name = Input::get('name');
            $app_menu->href = Input::get('href');
            $app_menu->description = Input::get('description');
            $app_menu->save();
            return Redirect()->route('appmenu.index');
        }
        else{
            $app_menu = new AppMenu;
            $app_menu->name = Input::get('name');
            $app_menu->href = Input::get('href');
            $app_menu->description = Input::get('description');
            $app_menu->save();
            return Redirect()->route('appmenu.index');
        }
    } 
}
