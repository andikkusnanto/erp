<?php

	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\PurchaseOrderDetil;

class PurchaseOrderDetailController extends Controller
{
    public function destroy($id){
		PurchaseOrderDetail::findOrFail($id)->delete();
    }
}