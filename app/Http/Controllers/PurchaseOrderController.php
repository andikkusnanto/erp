<?php

    namespace App\Http\Controllers;
    use DB;
    use Session;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\View;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Redirect;
    use App\Http\Requests\PurchaseOrderRequest;
    use App\PurchaseOrderDetail;
    use App\ExternalProvider;
    use App\ProductServices;
    use App\PurchaseOrder;
    use App\Uom;

class PurchaseOrderController extends Controller
{
    public function index(Request $request){
        $first_date = Input::get('date_first');
        $last_date = Input::get('date_last');
        if(isset($first_date) && isset($last_date)){
            $purchase_order = PurchaseOrder::where('date','>=',$first_date)->where('date','<=',$last_date)->orderBy('external_provider_id', 'asc')->orderBy('date', 'asc')->paginate(10);
        }else{
            $purchase_order = PurchaseOrder::orderBy('external_provider_id', 'asc')->orderBy('date', 'asc')->paginate(10);
        }
        return View::make('layouts/dashboard',array())->nest('content', 'purchaseorders.index',array('data'=>$purchase_order));
    }

    public function financialReport(Request $request){
        $first_date = Input::get('date_first');
        $last_date = Input::get('date_last');
        if(isset($first_date) && isset($last_date)){
            $purchase_order = PurchaseOrder::where('date','>=',$first_date)->where('date','<=',$last_date)->orderBy('external_provider_id', 'asc')->orderBy('date', 'asc')->paginate(10);
        }else{
            $purchase_order = PurchaseOrder::orderBy('external_provider_id', 'asc')->orderBy('date', 'asc')->paginate(10);
        }
        return View::make('layouts/dashboard',array())->nest('content', 'purchaseorders.financialreport',array('data'=>$purchase_order));
    }

    public function create(){
        $purchase_orders = PurchaseOrder::all();
        $external_providers = ExternalProvider::all();
        $options = array(
            'purchase_orders'=>$purchase_orders,
            'external_providers'=>$external_providers,
            );
        return View::make('layouts/dashboard',array())->nest('content', 'purchaseorders.create',$options);
    }

    public function store(ProductRequest $request){
        PurchaseOrder::create($request->all());
        return redirect()->route('purchaseorders.index')->with('success','Item created successfully');
    }

    public function totals(Request $request){
        $purchase_order_detail_amount = DB::table('purchase_order_details')
            ->where('purchase_order_id', '=', $request->id)
            ->sum('amount');
        return response()->json($purchase_order_detail_amount);
    }

    public function show(Request $request, $id){
        $purchase_order = PurchaseOrder::findOrFail($id);
        $purchase_order_detail_amount = DB::table('purchase_order_details')
            ->join('purchase_orders', 'purchase_order_details.purchase_order_id', '=', 'purchase_orders.id')
            ->where('purchase_orders.id', '=', $id)
            ->sum('purchase_order_details.amount');
        $options = array(
            'purchase_order'=>$purchase_order,
            'purchase_order_detail_amount'=>$purchase_order_detail_amount,
            );
        return view::make('layouts/dashboard',array())->nest('content', 'purchaseorders.show',$options);
    }

    public function edit(Request $request, $id){
        $purchase_order = PurchaseOrder::findOrFail($id);
        $purchase_order_detail = PurchaseOrderDetail::where('purchase_order_id','=',$id)->get();
        $external_providers = ExternalProvider::all();
        $product_services = ProductServices::all();
        $uoms= Uom::all();
        $options = array(
            'purchase_order'=>$purchase_order,
            'purchase_order_detail'=>$purchase_order_detail,
            'external_providers'=>$external_providers,
            'product_services'=>$product_services,
            'uoms'=>$uoms,
        );
        return View::make('layouts/dashboard',array())->nest('content', 'purchaseorders.edit',$options);
    }

    public function update(ProductRequest $request, $id){
        PurchaseOrder::findOrFail($id)->update($request->all());
        return redirect()->route('product.index')->with('success','Item updated successfully');
    }

    public function destroy($id){
        try{
            $purchase_order_detail = PurchaseOrderDetail::where('purchase_order_id',$id)->delete();
            $purchase_order= PurchaseOrder::where('id',$id)->delete();
            Session::flash('flash_message', 'Data has been deleted.');
            return redirect()->route('purchaseorder.index');
        }                   
        catch(\Exception $e){
            return redirect()->route('purchaseorder.index')->with('flash_warning','Data has been deleted.');
        }
    }

    public function search(Request $request){
        $query = $request->get('search');
        $purchase_order = PurchaseOrder::where('product_name', 'LIKE', '%' . $query . '%')->orWhere('spesification', 'LIKE', '%' . $query . '%')->orWhere('product_code', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('products.index',compact('purchase_order'))->with('b', ($request->input('page', 1) - 1) * 5);
    }   

    public function getExternalProvider(){
        $q = Input::get('q');
        $external_provider = ExternalProvider::where('expname', 'like', '%'.$q.'%')->orderBy('expname', 'asc')->limit(10)->get();
        $array = array();
        foreach($external_provider as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->expname
                );
        }
        echo json_encode($array);
    }

    public function getProductService(){
        $q = Input::get('q');
        $product_service = ProductServices::where('product_name', 'like', '%'.$q.'%')->orderBy('product_name', 'asc')->limit(10)->get();
        $array = array();
        foreach($product_service as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->product_name
                );
        }
        echo json_encode($array);
    }

    public function getUom(){
        $q = Input::get('q');
        $uom = Uom::where('name', 'like', '%'.$q.'%')->orderBy('name', 'asc')->limit(10)->get();
        $array = array();
        foreach($uom as $row){
            $array[] = array(
                'id'=>$row->id,
                'text'=>$row->name
                );
        }
        echo json_encode($array);
    }

    public function getSave(PurchaseOrderRequest $request){
        $id = Input::get('id');
        DB::beginTransaction();
        try {
            if($id){
                $po_source = Input::get('po_source');
                $po_number = Input::get('po_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }
                $term = Input::get('term');
                $idx=Input::get('counterx');
                $attention = Input::get('attention');
                $reff = Input::get('reff');
                $spesial_instruction = Input::get('spesial_instruction');
                $purchase_order = PurchaseOrder::findOrFail($id);
                $purchase_order->po_source = $po_source;
                $purchase_order->po_number = $po_number;
                $purchase_order->date = $date;
                $purchase_order->external_provider_id = $external_provider_id;
                $purchase_order->term = $term;
                $purchase_order->attention = $attention;
                $purchase_order->reff = $reff;
                $purchase_order->spesial_instruction = $spesial_instruction;
                $purchase_order->save();
                for($i=0;$i<=$idx-1;$i++){
                    $product_service_id = Input::get('product_service_id'.$i);
                    $purchase_order_id = $purchase_order->id;
                    $check_record = PurchaseOrderDetail::where('purchase_order_id','=',$purchase_order->id)->where('product_service_id','=',$product_service_id)->first();
                    if(empty($check_record)){
                        $purchase_order_detail = new PurchaseOrderDetail;
                        $purchase_order_detail->purchase_order_id = $purchase_order->id;
                        $purchase_order_detail->po_number = $po_number;
                        $purchase_order_detail->product_service_id = $product_service_id;
                        $product_service = ProductServices::find($product_service_id);
                        $purchase_order_detail->product_code = $product_service->product_code;
                        $purchase_order_detail->qty =  Input::get('qty'.$i);
                        $purchase_order_detail->uom_id =Input::get('uom_id'.$i);  
                        $purchase_order_detail->unit_price =Input::get('unit_price'.$i); 
                        $purchase_order_detail->amount =Input::get('qty'.$i)*Input::get('unit_price'.$i);
                        $purchase_order_detail->save();
                    }else{
                        $purchase_order_detail = PurchaseOrderDetail::where('purchase_order_id','=',$purchase_order->id)->where('product_service_id','=',$product_service_id)->first();
                        $purchase_order_detail->purchase_order_id = $purchase_order->id;
                        $purchase_order_detail->po_number = $po_number;
                        $purchase_order_detail->product_service_id = $product_service_id;
                        $product_service = ProductServices::find($product_service_id);
                        $purchase_order_detail->product_code = $product_service->product_code;
                        $purchase_order_detail->qty =  Input::get('qty'.$i);
                        $purchase_order_detail->uom_id =Input::get('uom_id'.$i);  
                        $purchase_order_detail->unit_price =Input::get('unit_price'.$i); 
                        $purchase_order_detail->amount =Input::get('qty'.$i)*Input::get('unit_price'.$i);
                        $purchase_order_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('purchaseorder.index');
            }else{
                $po_source = Input::get('po_source');
                $po_number = Input::get('po_number');
                $date = Input::get('date');
                $external_provider_id = Input::get('external_provider_id');
                if($external_provider_id){
                    $external_provider_id = Input::get('external_provider_id');
                }else{
                    $external_provider_id = NULL;
                }
                $term = Input::get('term');
                $idx=Input::get('counterx');
                $attention = Input::get('attention');
                $reff = Input::get('reff');
                $spesial_instruction = Input::get('spesial_instruction');
                $purchase_order = new PurchaseOrder;
                $purchase_order->po_source = $po_source;
                $purchase_order->po_number = $po_number;
                $purchase_order->date = $date;
                $purchase_order->external_provider_id = $external_provider_id;
                $purchase_order->term = $term;
                $purchase_order->attention = $attention;
                $purchase_order->reff = $reff;
                $purchase_order->spesial_instruction = $spesial_instruction;
                $purchase_order->save();
                for($j=0;$j<=$idx;$j++){
                    $product_service_id = Input::get('product_service_id'.$j);
                    $check_record = PurchaseOrderDetail::where('purchase_order_id','=',$purchase_order->id)->where('product_service_id','=',$product_service_id)->first();
                    if(empty($check_record)){
                        $purchase_order_detail = new PurchaseOrderDetail;
                        $purchase_order_detail->purchase_order_id = $purchase_order->id;
                        $purchase_order_detail->po_number = $po_number;
                        $purchase_order_detail->product_service_id = $product_service_id;
                        $product_service = ProductServices::find($product_service_id);
                        $purchase_order_detail->product_code = $product_service->product_code;
                        $purchase_order_detail->qty =  Input::get('qty'.$j);
                        $purchase_order_detail->uom_id =Input::get('uom_id'.$j);  
                        $purchase_order_detail->unit_price =Input::get('unit_price'.$j); 
                        $purchase_order_detail->amount =Input::get('qty'.$j)*Input::get('unit_price'.$j);
                        $purchase_order_detail->save();
                    }
                }
                DB::commit();
                return Redirect()->route('purchaseorder.index');
            }
        }
        catch (\Exception $e){
            DB::rollback();
            return back()->withInput()->withErrors('Data is invalid');
        }
    }
}
