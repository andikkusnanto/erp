<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class ExternalProviderRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
       if($this->method() == 'PATCH')
        {
            $expcode_rules='required|string|max:50|unique:external_providers,expcode,'.$this->get('id');
            $expname_rules='required|string|max:100|unique:external_providers,expname,'.$this->get('id');
        }
        else
        {
            $expcode_rules='required|string|max:50|unique:external_providers,expcode';
            $expname_rules='required|string|max:100|unique:external_providers,expname';
        } 

        return [
            'expcode'=>$expcode_rules,
            'expname'=>$expname_rules,
            ];
    }
}
