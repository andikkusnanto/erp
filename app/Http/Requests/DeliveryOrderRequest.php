<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class DeliveryOrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
     if($this->method() == 'POST')
     {
        $do_number_rules='required|string|max:50|unique:delivery_orders,do_number,'.$this->get('id');
        $external_provider_id_rules='required|integer';
    }
    else
    {
        $do_number_rules='required|string|max:50|unique:delivery_orders,do_number';
        $external_provider_id_rules='required|integer';
    } 

    return [
    'do_number'=>$do_number_rules,
    'external_provider_id'=>$external_provider_id_rules,
    ];
    }
}
