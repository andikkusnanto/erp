<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class AppMenuRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
       if($this->method() == 'POST')
        {
            $name_rules='required|string|max:100|unique:app_menus,name,'.$this->get('id');
            $href_rules='required|string|unique:app_menus,href,'.$this->get('id');
            $description_rules='required|string';
        }
        else
        {
            $name_rules='required|string|max:100|unique:app_menus,name';
            $href_rules='required|string|unique:app_menus,href';
            $description_rules='required|string';            
        } 

        return [
            'name'=>$name_rules,
            'href'=>$href_rules,
            'description'=>$description_rules,
            ];
    }
}