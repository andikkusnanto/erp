<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class PaymentVoucherRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->method() == 'POST')
        {
            $pv_number_rules='required|string|max:50|unique:payment_vouchers,pv_number,'.$this->get('id');
            $external_provider_id_rules='required|integer:payment_vouchers,external_provider_id';
        }
        else
        {
            $pv_number_rules='required|string|max:50|unique:payment_vouchers,pv_number';
            $external_provider_id_rules='required|integer:payment_vouchers,external_provider_id';
        } 

        return [
            'pv_number'=>$pv_number_rules,
            'external_provider_id'=>$external_provider_id_rules,
            ];
    }
}
