<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class ProductTypeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

 
    public function rules()
    {
       if($this->method() == 'PATCH')
        {
            $type_name_rules='required|string|max:50|unique:product_types,type_name,'.$this->get('id');
        }
        else
        {
            $type_name_rules='required|string|max:50|unique:product_types,type_name';
        } 

        return [
            'type_name'=>$type_name_rules,
            ];
    }
}
