<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
     if($this->method() == 'POST')
     {
        $invoice_number_rules='required|string|max:50|unique:invoices,invoice_number,'.$this->get('id');
        $external_provider_id_rules='required|integer';
    }
    else
    {
        $invoice_number_rules='required|string|max:50|unique:invoices,invoice_number';
        $external_provider_id_rules='required|integer';
    } 

    return [
    'invoice_number'=>$invoice_number_rules,
    'external_provider_id'=>$external_provider_id_rules,
    ];
    }
}
