<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Contracts\Validation\Validator;

class PurchaseOrderRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        if($this->method() == 'POST')
        {
            $po_number_rules='required|string|max:50|unique:purchase_orders,po_number,'.$this->get('id');
            $external_provider_id_rules='required|integer:purchase_orders,external_provider_id,'.$this->get('id'); 
        }
        else
        {
            $po_number_rules='required|string|max:50|unique:purchase_orders,po_number';
            $external_provider_id_rules='required|integer:purchase_orders,external_provider_id';
        } 

        return [
            'po_number'=>$po_number_rules,
            'external_provider_id'=>$external_provider_id_rules,
            ];
    }
}
