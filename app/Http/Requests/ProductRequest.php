<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
       if($this->method() == 'POST')
        {
            $product_code_rules='required|string|max:30|unique:product_services,product_code,'.$this->get('id');
            $product_name_rules='required|string|max:30|unique:product_services,product_name,'.$this->get('id');
        }
        else
        {
            $product_code_rules='required|string|max:30|unique:product_services,product_code';
            $product_name_rules='required|string|max:30|unique:product_services,product_name';
        } 

        return [
            'product_code'=>$product_code_rules,
            'product_name'=>$product_name_rules,
            ];
    }
}
