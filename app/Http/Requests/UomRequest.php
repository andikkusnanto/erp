<?php

    namespace App\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

class UomRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
       if($this->method() == 'PATCH')
        {
            $name_rules='required|string|max:100|unique:uoms,name,'.$this->get('id');
        }
        else
        {
            $name_rules='required|string|max:100|unique:uoms,name';
        } 

        return [
            'name'=>$name_rules,
            ];
    }
}
