<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProductServices extends Model{
	protected $table = 'product_services';
	public $fillable = ['product_type_id','product_code','product_name','spesification'];

	public function productType(){
		return $this->belongsTo('App\ProductType', 'product_type_id');
	}

	public function purchaseOrderDetail(){
        return $this->hasMany('App\PurchaseOrderDetail','product_service_id');
    }

    public function deliveryOrderDetail(){
        return $this->hasMany('App\DeliveryOrderDetail','product_service_id');
    }
}
