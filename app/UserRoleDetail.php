<?php

	namespace App;
	use Illuminate\Database\Eloquent\Model;

class UserRoleDetail extends Model
{
    protected $table = 'user_role_details';
    protected $fillable = ['user_role_id','app_menu_id','privileges','description'];

    public function userRole(){
        return $this->belongsTo('App\UserRole','user_role_id');
    }

    public function appMenu(){
        return $this->belongsTo('App\AppMenu','app_menu_id');
    }
}
