<?php

	namespace App;
	use Illuminate\Database\Eloquent\Model;

class PaymentVoucherDetail extends Model
{
    protected $table = 'payment_voucher_details';
    public $fillable = ['payment_voucher_id','pv_number','invoice_id','amount','remark'];

	public function paymentVoucher(){
        return $this->belongsTo('App\PaymentVoucher','payment_voucher_id');
    }

    public function invoiceDetail(){
        return $this->belongsTo('App\InvoiceDetail','invoice_id');
    }

}
