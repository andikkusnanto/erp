<?php
    namespace App;
    use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    protected $table = 'uoms';
    protected $fillable = ['name','remark'];

    public function purchaseOrderDetail(){
        return $this->hasMany('App\PurchaseOrderDetail','uom_id');
    }

    public function deliveryOrderDetail(){
        return $this->hasMany('App\DeliveryOrderDetail','uom_id');
    }

	public function invoiceDetail(){
        return $this->hasMany('App\InvoiceDetail','uom_id');
    }
}
