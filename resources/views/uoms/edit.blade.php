@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="col-md-12">
    <div class="col-md-6"> 
        <h4><b>Edit Uom</b></h4>
        {!! Form::model($uom, ['method' => 'PATCH','route' => ['uom.update', $uom->id]]) !!}
        <h2><a href="{{ route('uom.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
        <div class="form-group">
            <strong>id:</strong>
            {!! Form::text('id', null, array('placeholder' => 'id','class' => 'form-control','readonly')) !!}
        </div>
        <div class="form-group">
            <strong>Name : </strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}        
        </div>
        <div class="form-group">
            <strong>Remark : </strong>
            {!! Form::text('remark', null, array('placeholder' => 'Remark','class' => 'form-control')) !!}
        </div>
        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Submit </button>
        {!! Form::close() !!}
    </div>
</div>
