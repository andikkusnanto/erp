<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>
<div class="col-md-12">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
</div>

<div class="col-md-12">
    <h4><b>Unit of Material</b></h4>
    <table class="table table-striped table-hover" id="TBLuom">
        <thead>
            <tr>
                <tr> 
                <th colspan="4">
                        <h3 class="pull-right">
                            <a style="color: green" href="{{ route('uom.create') }}">
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </h3>
                    </th>
                </th>
            </th>
        </th>
    </tr>
    <th>No</th>
    <th>Name</th>
    <th>Remark</th>
    <th>Action</th>
</tr>
</thead>
<tbody>
    @foreach ($uoms as $key => $item)
    <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $item->name }}</td>
        <td>{{ $item->remark}}</td>
        <td>
            <a href="{{ route('uom.edit',$item->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
            <a href="{{ route('uom.show',$item->id) }}"><i class="glyphicon glyphicon-search"></i></a>
            <a style="color: red" href="{{ route('uom.destroy',$item->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
</div>
<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLuom').DataTable();
    } );
</script>