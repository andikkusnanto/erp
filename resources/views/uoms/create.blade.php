@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="col-md-6">
    <h4><b>Create New Item</b></h4>
    <h2><a href="{{ route('uom.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
    {!! Form::open(array('route' => 'uom.store','method'=>'POST')) !!}
    <div class="form-group">
        <strong>Name : </strong>
        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
    </div>
    <div class="form-group">
        <strong>Remark : </strong>
        {!! Form::text('remark', null, array('placeholder' => 'Remark','class' => 'form-control')) !!}
    </div>
    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Submit </button>
    {!! Form::close() !!}
</div>
