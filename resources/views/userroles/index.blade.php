<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>
<div class="col-md-12">
    <h4><b>User Roles</b></h4>
</div>
<div class="col-md-12">
<form method="GET" href="{{route('userrole.index')}}">
    <div class="col-md-9">
        
        
    </div>
</form>
<br>
<?php 
$no = 1; 
if(isset($_GET['page'])){
    $no = ($_GET['page']*10)-9;
}
?>
@if (Session::has('message'))

<div class="alert alert-success">
    <i class="fa fa-exclamation-circle"></i><small>  {{Session::get('message')}} !!</small>
    <button type="button" class="close" data-dismiss="alert">
        ×
    </button>
</div>
@endif
<br>
<table class="table table-striped table-hover" id="TBLuserroles">
<thead>
    <tr>
        <tr>
            <th colspan="11">
                <h3 class="pull-right">
                    <a style="color: green" href="{{ route('userrole.create') }}">
                        <span class="glyphicon glyphicon-plus" class="pull-right"></span>
                    </a>
                </h3>
            </th>
        </tr>
        <th style="text-align:center;">#</th>
        <th>User Role</th>
        <th>Description</th>
        <th style="text-align:center;">Action</th>
    </tr>
    </thead>
    <tbody>
        <!-- <?php $i=0; ?> -->
        <?php foreach($user_role as $row): ?>
            <tr>
                <!-- {{++$i}}   -->
                <td><?php echo $no;?></td>
                <td><?php echo $row->role;?></td>
                <td><?php echo $row->description;?></td>
                <td>
                    <a href="{{ route('userrole.edit',$row->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{{ route('userrole.show',$row->id) }}"><i class="glyphicon glyphicon-search"></i></a>
                    <a style="color: red" href="{{ route('userrole.destroy',$row->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
                </td>
            </tr>

            <?php $no++; EndForeach; ?>
        </tbody>
    </table>
</div>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#TBLuserroles').DataTable();
        } );
    </script>
