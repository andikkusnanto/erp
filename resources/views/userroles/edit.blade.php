<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>
        <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
            <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}">
            <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
                <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
                <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
                <script src="{{ asset('js/summernote/summernote.js') }}"</script> 
                    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
                        <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
                            <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 
                            <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
                            <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
                            <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
                            <script src="{{ asset('js/jquery/select2.js') }}"</script>
                                <script src="{{ asset('js/jquery/date.js') }}"</script>
                                    <script>
                                        window.Laravel = {!! json_encode([
                                            'csrfToken' => csrf_token(),
                                            ]) !!};
                                        </script>
                                    </head>

                                    <div class="col-lg-12">
                                        <h4><b>User Role</b></h4>
                                        <h3><a href="{{ route('userrole.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h3>
                                    </div>
                                    <form class="form-horizontal" method="POST" action="{{ route('userrole.getsave','4') }}">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                        <div class="form-group required">
                                            <label class="control-label" hidden="true">counterx</label>
                                            <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group required">
                                                <div class="col-md-5">
                                                    <label class="control-label">id</label>
                                                    <input type="text" id="id" value="<?php echo $user_role->id;?>" name="id" class="form-control" readonly="true"/>
                                                </div>

                                                <div class="col-md-5">
                                                    <label class="control-label">User Role</label> 
                                                    <input type="text" id="role" value="<?php echo $user_role->role;?>" name="role" class="form-control" required/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group required">
                                                <div class="col-md-5">
                                                    <label class="control-label">Description</label>
                                                    <input type="text" id="description" value="<?php echo $user_role->description;?>" name="description" class="form-control" required/> 
                                                </div>

                                            </div>
                                        </div>
                                            
                                            <div id="data-userrole"></div>
                                            <table class="table table-bordered table-hover" id="TData">
                                                <tr>
                                                    <td colspan="7">
                                                        <label># Details Of User Role</label>
                                                        <h3><a href="javascript:void(0)" onclick="AddURoleDetil()" title="Add New User Role Detil" style="color: green ">
                                                            <span class="glyphicon glyphicon-plus pull-right"></span>
                                                        </a></h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>App Menu</th>
                                                    <th style="text-align:center;">Privileges</th>
                                                    <th style="text-align:center;">Description</th>
                                                    <th style="text-align:center;">Action</th>
                                                </tr>

                                                <script type="text/javascript">
                                                    var idx=0;
                                                </script>

                                                <?php $v = 0; foreach($user_role_detail as $data_detil): ?>

                                                <tr class="index" id="<?php echo $v;?>">
                                                    <td><select id="app_menu_id<?php echo $v;?>" name="app_menu_id<?php echo $v;?>" class="form-control">
                                                        <option></option>
                                                        <?php foreach($app_menus as $c): ?>
                                                            <?php if($c->id==$data_detil->app_menu_id): ?>
                                                                <option value="<?php echo $c->id;?>" selected><?php echo $c->name;?></option>
                                                                <?php Else: ?>
                                                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                                                <?php EndIf; ?>     
                                                                <?php EndForeach; ?>
                                                            </select></td>

                                                            <td><input type="text" value="<?php echo $data_detil->privileges;?>" id="privileges<?php echo $v;?>" name="privileges<?php echo $v;?>" class="form-control" /></td>

                                                            <td><input type="text" style="text-align:right;" value="<?php echo $data_detil->description;?>" id="description<?php echo $v;?>" name="description<?php echo $v;?>" class="form-control" /></td>

                                                                    <script type="text/javascript">
                                                                        var nm = $('#v_jv').val()

                                                                        $('#app_menu_id'+idx).select2({

                                                                            ajax: {
                                                                                placeholder: 'App Menu',    
                                                                                url: '<?php echo url('/userroles/getAppMenu');?>',
                                                                                dataType: 'json',
                                                                                quietMillis: 100,
                                                                                data: function (term) {
                                                                                    return {
                                                                                q: term, // search term
                                                                            };
                                                                        },
                                                                        results: function (data) {
                                                                            var myResults = [];
                                                                            $.each(data, function (index, item) {
                                                                                myResults.push({
                                                                                    'id': item.id,
                                                                                    'text': item.text
                                                                                });
                                                                            });
                                                                            return {
                                                                                results: myResults
                                                                            };
                                                                        },
                                                                        minimumInputLength: 3
                                                                    }
                                                                });

                                                                        
                                                                    </script>

                                                                    <td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)"><i class="glyphicon glyphicon-trash"></i></a></td>


                                                                    <script type="text/javascript">
                                                                        idx=idx+1;
                                                                    </script>
                                                                </tr>

                                                                <?php $v++; EndForeach; ?>

                                                            </table>
                                                            <div class="pull-right">
                                                                <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><span class="glyphicon glyphicon-refresh"></span></button>
                                                                <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span></button>

                                                            </div>
                                                        </form>





                                                        <script type="text/javascript">

                                                            if (idx<=0) {idx=0} else {}

                                                                $(document).ready(function(){
                                                                });

                                                            function AddURoleDetil(){

                                                                var row = $('#TData tbody tr').length;

                                                                if(row>0){
                                                                    var index = $('.index').last().attr('id');
                                                                    index = parseInt(index);
                                                                    row = parseInt(row);
                                                                    row = index+1;
                                                                }

                                                                $('#counterx').val(idx);

                                                                document.getElementById("counterx").innerHTML=idx;
                                                                var x = document.getElementById("counterx");

                                                                var html = '<tr id="'+idx+'" class="index">';
                                                                html += '<td><input type="hidden" id="app_menu_id'+row+'" name="app_menu_id'+idx+'"  class="form-control" required/></td>'; 
                                                                html += '<td><input type="text" id="privileges'+row+'" name="privileges'+idx+'"  class="form-control" /></td>';
                                                                 
                                                                html += '<td><input type="text" id="description'+row+'" name="description'+idx+'"  class="form-control" /></td>';
                                                                
                                                                html += '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="glyphicon glyphicon-trash"><i class="fa fa-times"></i></a></td>';
                                                                html += '</tr>';

                                                                $('#TData tbody').append(html);

                                                                idx=idx+1;
                                                                $('#counterx').val(idx);

                                                                $('#app_menu_id'+row).select2({

                                                                    ajax: {

                                                                        placeholder: 'App Menu',    

                                                                        url: '<?php echo url('/userroles/getappmenu');?>',

                                                                        dataType: 'json',
                                                                        quietMillis: 100,
                                                                        data: function (term) {
                                                                            return {
                                        q: term, // search term
                                    };
                                },
                                results: function (data) {
                                    var myResults = [];
                                    $.each(data, function (index, item) {
                                        myResults.push({
                                            'id': item.id,
                                            'text': item.text
                                        });
                                    });
                                    return {
                                        results: myResults
                                    };
                                },
                                minimumInputLength: 3
                            }
                            
                        });

                                                               


                                                            }


                                                            function deleteRow(btn) {
                                                              var row = btn.parentNode.parentNode;
                                                              row.parentNode.removeChild(row);
                                                              idx=idx-1;
                                                              if (idx<=0) {idx=0} else {}
                                                                  $('#counterx').val(idx);

                                                          }

                                                          
        
        $('#counterx').val(idx); 
        // AddPoDetil();
        //uom();

    </script>