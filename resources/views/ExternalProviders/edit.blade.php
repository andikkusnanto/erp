@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::model($data, ['method' => 'PATCH','route' => ['externalprovider.update', $data->id]]) !!}
<div class="col-md-12">
    <div class="col-md-4 col-xs-4 konten">
        <h4><b>Edit Form External Provider</b></h4>
        <h2><a href="{{ route('externalprovider.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
        <div class="form-group">
            <strong>id:</strong>
            {!! Form::text('id', null, array('placeholder' => 'id','class' => 'form-control','readonly')) !!}
            <strong>External Provider Code:</strong>
            {!! Form::text('expcode', null, array('placeholder' => 'External Provider Code','class' => 'form-control')) !!}        
            <strong>External Provider Name:</strong>
            {!! Form::text('expname', null, array('placeholder' => 'Extrnal Provider Name','class' => 'form-control')) !!}
            <strong>Person In Charge:</strong>
            {!! Form::text('pic', null, array('placeholder' => 'Person In Charge','class' => 'form-control')) !!}
            <strong>Phone:</strong>
            {!! Form::text('phone', null, array('placeholder' => 'Phone','class' => 'form-control')) !!}
            <strong>E- Mail:</strong>
            {!! Form::text('email', null, array('placeholder' => 'E - Mail','class' => 'form-control')) !!}
            <strong>Maps:</strong>
            {!! Form::text('maps', null, array('placeholder' => 'Maps','class' => 'form-control')) !!}
            <strong>Address:</strong>
            {!! Form::text('address', null, array('placeholder' => 'Address','class' => 'form-control')) !!}
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Submit </button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
