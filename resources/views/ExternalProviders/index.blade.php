<html>
<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>
<body>
    <div class="col-md-12">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    </div>
    @endif
    <div class="col-md-12">
        <h4><b>External Provider</b></h4>
        <table class="table table-striped table-hover" id="TBLexternalprovider">
            <thead>
                <tr>
                    <th colspan="8">
                        <h3 class="pull-right">
                            <a style="color: green" href="{{ route('externalprovider.create') }}">
                                <span class="glyphicon glyphicon-plus"></span>
                            </a>
                        </h3>
                    </th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>External Provider Code</th>
                    <th>External Provider Name</th>
                    <th>Person In Charge</th>
                    <th>E - mail</th>
                    <th>Maps</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $datas)
                    <tr>
                        <td align="center">{{ $key+1 }}</td>
                        <td>{{ $datas->expcode }}</td>
                        <td>{{ $datas->expname }}</td>
                        <td>{{ $datas->pic }}</td>
                        <td>{{ $datas->email }}</td>
                        <td>{{ $datas->maps }}</td>
                        <td>{{ $datas->address }}</td>
                        <td>
                            <a href="{{ route('externalprovider.edit',$datas->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="{{ route('externalprovider.show',$datas->id) }}"><i class="glyphicon glyphicon-search"></i></a>
                            <a style="color: red" href="{{ route('externalprovider.destroy',$datas->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>

<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLexternalprovider').DataTable();
    } );
</script>
