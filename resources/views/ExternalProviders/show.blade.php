<div class="col-md-12">
    <div class="col-md-6">
        <h2> Show List External Provider</h2>
        <h2><a href="{{ route('externalprovider.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
        <div class="form-group">
            <strong>Id :</strong>
            {{ $data->id }}
        </div>
        <div class="form-group">
            <strong>External Provider Code :</strong>
            {{ $data->expcode }}
        </div>
        <div class="form-group">
            <strong>Exernal Provider Name :</strong>
            {{ $data->expname }}
        </div>
        <div class="form-group">
            <strong>Person In Charge :</strong>
            {{ $data->pic }}
        </div>
        <div class="form-group">
            <strong>Phone :</strong>
            {{ $data->phone }}
        </div>
        <div class="form-group">
            <strong>E - Mail :</strong>
            {{ $data->email }}
        </div>
        <div class="form-group">
            <strong>Maps :</strong>
            {{ $data->maps }}
        </div>
        <div class="form-group">
            <strong>Address :</strong>
            {{ $data->address }}
        </div>
</div>
</div>