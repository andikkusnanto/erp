<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
	<meta charset="UTF-8" />
	<title>ERP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
	<script type="text/javascript">
		var base_url = '<?php echo url('/');?>';
	</script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script> src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
	<script> src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
	<link rel="stylesheet" href="/js/jquery/datetimepicker/bootstrap-datetimepicker.min.css">
	<script> src="{{ asset('js/jquery/select2.js') }}"</script>
	<script> src="{{ asset('js/jquery/date.js') }}"</script>
	<link rel="stylesheet" href="/css/select2.css">
	<link rel="stylesheet" href="/css/select2-bootstrap.css">
	<script> src="{{ asset('js/app.js') }}"</script>
	<script> src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
	<link rel="stylesheet" href="/js/bootstrap/less/bootstrap.less">
	<script> src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
	<link rel="stylesheet" href="/js/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/js/summernote/summernote.css">
	<script> src="{{ asset('js/summernote/summernote.js') }}"</script>
	<link rel="stylesheet" href="/css/stylesheet.css">
	<script> src="{{ asset('js/common.js') }}"</script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>
<body>
	<div id="container">
		<header id="header" class="navbar navbar-static-top">
			<div class="navbar-header">
				<a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
			</div>

			<ul class="nav pull-right">
				<li>
					<a  href="{{ url('/logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out"  aria-hidden="true"></span> Logout </a>
					<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</li>
			</ul>
		</header>
		<nav id="column-left">
			<div id="profile">
				<div>
					<a class="dropdown-toggle" data-toggle="dropdown"></a>
				</div>
			</div>
			<ul id="menu">
				<li id="dashboard">
					<a href="{{route('home.default')}}"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a>
				</li>
				<li id ="Catalog">
					<a class="parent"><i class="fa fa fa-folder-o fa-fw"></i> <span>Catalog</span></a>
					<ul>
					    <li id ="catalogchild" class="active"> 
					    	<a href="#">CATALOG</a>
					    </li>
					</ul>
				</li>
				<li>
					<a class="parent"><i class="fa fa-tasks fa-fw"></i> <span>Tasks</span></a>
					<ul>
						<li id ="taskchild" class="active"> 
					    	<a href="#">TASK</a>
					    </li>
					</ul>
				</li>

				<li>
					<a class="parent"><i class="fa fa-line-chart fa-fw"></i> <span>Reports</span></a>
					<ul>
						<li id ="reportchild" class="active"> 
					    	<a href="#">REPORT</a>
					    </li>

					</ul>
				</li>
				<li>
					<a class="parent"><i class="fa fa-users" aria-hidden="true"></i> <span>System</span></a>
					<ul>
						<li id ="systemchild" class="active"> 
					    	<a href="#">SYSTEM</a>
					    </li>
					</ul>
				</li>
			</ul>
		</nav>

		<div id="content">
			<div class="page-header">
				<div class="container-fluid">
					<h1>ERP</h1>
					<ul class="breadcrumb">
						<li>
							<a href="#">Enterprise Resources Planning</a>
						</li>
						<li>
							<a href="#">&copy; 2017</a>
						</li>
					</ul>
				</div>
			</div>
			<?php if(isset($content)): echo $content; EndIf; ?>
			</div>
			<div class="col-md-12">
				<footer id="footer">
					<a href="#">Dream Smart</a> &copy; <?php echo date('Y');?> All Rights Reserved.
					<br />
					Version 1.1.0
				</footer>
			</div>
		</div>
		<div class="modal fade" id="loading">
			<div class='modal-dialog'>
				<div class='modal-body'>
					<div class="sk-wave">
						<div class="sk-rect sk-rect1"></div>
						<div class="sk-rect sk-rect2"></div>
						<div class="sk-rect sk-rect3"></div>
						<div class="sk-rect sk-rect4"></div>
						<div class="sk-rect sk-rect5"></div>
					</div>
				</div>
			</div>
		</div>

	</body>
	</html>

	<script type="text/javascript">
	function getmenu(prmemail)
		{
			var prmemail = prmemail;
			$.ajax({
				type:'get',
				url: '<?php echo url('/userrolemans/getUserMenu');?>',
				data:{'email':prmemail},
				datatype:'json',
				success:function(data)
					{   
						for (var i=0; i<JSON.parse(data).length; i++) {
						    
						    parsed_data = JSON.parse(data);
						    var prmhref = parsed_data[i].href;
                            var prmdescription = parsed_data[i].description;
							var linkFromJson = "<a>" + parsed_data[i].menu_name + " </a>";
							linkFromJson = $(linkFromJson).attr("href",'../../' + prmhref);
							$("#"+ prmdescription).append(linkFromJson);
						}
                        },
				error:function()
					{
					}
			});
		}
</script>
	<script type="text/javascript"> 
    var prmemail='{{ Auth::user()->email }}';
    var prmparent='taskchild'
    catalog_menu = getmenu(prmemail); 
</script>

