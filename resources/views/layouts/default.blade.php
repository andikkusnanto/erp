<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
       
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>


    
<!--     <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}"</script>
    <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
    <script src="{{ asset('js/summernote/summernote.js') }}"</script> -->

 
    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
    <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 

    <!-- <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
    <script src="{{ asset('js/jquery/select2.js') }}"</script>
    <!-- <script src="{{ asset('js/jquery/date.js') }}"</script> -->
    <!-- <script src="{{ asset('js/common.js') }}" </script> -->




    <style>
        .konten{
            height: 300px;
        }
        .sidebar{
            height: 300px;
            padding-top: 0;
            word-spacing: -12px;
            font-size: 15px;
        }
        
    </style>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body>
<div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <span class="glyphicon glyphicon-user"> {{ Auth::user()->name }} <span class="caret"></span></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    
                                    <li>
                                        <a href="{{ url('/Profile') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('Profile-form').submit();">
                                            Profile
                                        </a>
                                        <form id="Profile-form" action="{{ url('/Profile') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                    <li>
                                        <a href="{{ url('/Settings') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('Settings-form').submit();">
                                            Settings
                                        </a>
                                        <form id="Settings-form" action="{{ url('/Settings') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>


                                    <li>
                                        <a href="{{ url('/help') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('help-form').submit();">
                                            Help
                                        </a>
                                        <form id="help-form" action="{{ url('/help') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>

            </div>
        </nav>
    <div class="col-md-3 col-xs-12 sidebar">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="{{route('home.default')}}"><span class="glyphicon glyphicon-home" aria-hidden="true"> &nbsp;&nbsp;Dashboard </span></a></li>

            <li><a href="{{route('deliveryorder.index')}}"><span class="glyphicon glyphicon-plane" aria-hidden="true">&nbsp;&nbsp; Delivery Order</span></a></li>
            
            <li><a href="#"><span class="glyphicon glyphicon-globe" aria-hidden="true">&nbsp;&nbsp; External Provider</span></a></li>

            <li><a href="{{route('invoice.index')}}"><span class="glyphicon glyphicon-usd" aria-hidden="true">&nbsp;&nbsp; Invoice </span></a></li>

            <li><a href="#"><span class="glyphicon glyphicon-list-alt" aria-hidden="true">&nbsp;&nbsp; Product Type </span></a></li>

            <li><a href="{{route('product.search')}}"><span class="glyphicon glyphicon-book" aria-hidden="true">&nbsp;&nbsp; Product and Service </span></a></li>

            <li><a href="{{route('purchaseorder.index')}}"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true">&nbsp;&nbsp; Purchase Order </span></a></li>

            <li><a href="{{route('uom.index')}}"><span class="glyphicon glyphicon-th" aria-hidden="true">&nbsp;&nbsp; Uom </span></a></li>

        </ul>

    </div>      
    @yield('content')
    
    </div>
    


</body>
</html>

