<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <!-- <script> src="{{ asset('js/jquery/jqvmap/jquery.vmap.js') }}"</script>
    <script> src="{{ asset('js/jquery/jqvmap/maps/jquery.vmap.world.js') }}"</script>
    <link rel="stylesheet" href="/js/jquery/jqvmap/jqvmap.css"> -->
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>
    <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}">
    <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
    <script src="{{ asset('js/summernote/summernote.js') }}"</script> 
    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
    <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
    <script src="{{ asset('js/jquery/select2.js') }}"</script>
    <script src="{{ asset('js/jquery/date.js') }}"</script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            ]) !!};
    </script>

</head>

<div class="container-fluid">
    <?php if (Session::has('message')): ?>
        <div class="alert alert-success">
            <i class="fa fa-exclamation-circle"></i><small> <?php echo Session::get('message'); ?>.</small>
            <button type="button" class="close" data-dismiss="alert">
                ×
            </button>
        </div>
    <?php EndIf; ?>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="tile">
                <div class="tile-heading">
                    Purchase Order
                </div>
                <div class="tile-body">
                    <i class="fa fa-users"></i>
                    <h2 class="pull-right"><?php echo number_format($total_po,0,",",".");?></h2> 
                </div>
                <div class="tile-footer">
                    <a href="<?php echo url('/purchaseorders/index');?>">View more..</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="tile">
                <div class="tile-heading">
                    Delivery Order
                </div>
                <div class="tile-body">
                    <i class="fa fa-plane"></i>
                    <h2 class="pull-right"><?php echo number_format($total_do,0,",",".");?></h2> 
                </div>
                <div class="tile-footer">
                    <a href="<?php echo url('/deliveryorders/index');?>">View more...</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="tile">
                <div class="tile-heading">
                    Invoice
                </div>
                <div class="tile-body">
                    <i class="fa fa-file-o"></i>
                    <h2 class="pull-right"><?php echo number_format($total_invoice,0,",",".");?></h2> 
                </div>
                <div class="tile-footer">
                    <a href="<?php echo url('/invoices/index');?>">View more...</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="tile">
                <div class="tile-heading">
                    Payment Voucher
                </div>
                <div class="tile-body">
                    <i class="fa fa-dollar"></i>
                    <h2 class="pull-right"><?php echo number_format($total_pv,0,",",".");?></h2> 
                </div>
                <div class="tile-footer">
                    <a href="<?php echo url('/paymentvouchers/index');?>">View more...</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart"></i> Charts Of <?php echo date('Y');?></h3>
                </div>
                <div class="panel-body">
                    <div id="placeholder" class="demo-placeholder" style="height:150px;"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-road"></i> Locations</h3>
                </div>
                <div class="panel-body">
                    <div id="vmap" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-external-link"></i> Financial Report </h3>
                </div>
                <div class="col-md-12">
                <table class="table table-striped table-hover" id="TBLfinancialreport">
                    <thead>
                        <tr>
                        <th style="text-align:left;">#</th>
                        <th style="text-align:left;">Purchase Order</th>
                        <th style="text-align:left;">Delivery Order</th>
                        <th style="text-align:left;">Invoice</th>
                        <th style="text-align:left;">Payment Voucher</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <?php $no=0; ?> -->
                        <?php foreach($financial_report as $row): ?>
                            <tr>
                                <!-- {{++$no}}   -->
                                <td><?php echo $no;?></td>
                                <td><?php echo $row->po_number . ': ' . $row->po_date . ', ' . $row->product_name . ', ' . number_format($row->po_amount,0,",",".");?></td>
                                <td><?php echo $row->do_number . ': ' . $row->do_date . ', ' . $row->do_remark;?></td>
                                <td><?php echo $row->invoice_number . ': ' . $row->invoice_date . ', ' . number_format($row->invoice_amount,0,",",".");?></td>
                                <td><?php echo $row->pv_number . ': ' . $row->pv_date . ', ' . $row->pv_remark;?></td>
                            </tr>
                        <?php $no++; EndForeach; ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>


</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLfinancialreport').DataTable({
            "bFilter": false
        });
    } );
</script>
