<div class="col-md-12">
    <h4><b>Payment Vouchers</b></h4>
    <h2><a href="<?php echo url('/paymentvouchers/index');?>" data-toggle="tooltip" title="Back To Grid"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
</div>
<form class="form-horizontal" method="POST" action="{{ route('paymentvoucher.getsave') }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-8">
                <label class="control-label">Payment Voucher Number</label>
                <input type="text" id="pv_number" value="<?php echo $payment_voucher->pv_number;?>" name="pv_number" class="form-control" readonly="true"/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <label class="col-sm-2 control-label" hidden="true">counterx</label>
            <div class="col-sm-6">
                <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-8">
             <label class="control-label">Date</label>
             <input type="date" value="<?php echo $payment_voucher->date;?>" id="date" name="date" class="form-control" readonly="true"/>
         </div>
     </div>
 </div>
 <div class="col-md-12">
     <div class="form-group required">
         <div class="col-md-8">
            <label class="control-label">External Provider</label>
            <input type="text" id="external_provider_id" value="<?php echo $payment_voucher->externalProvider->expname;?>" name="external_provider_id" class="form-control"  readonly="true"/>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group required">
        <div class="col-sm-8">
            <label class="control-label">Remark</label>
            <input type="text" id="remark" value="<?php echo $payment_voucher->remark;?>" name="remark" class="form-control" readonly="true"/>
        </div>
    </div>
</div>
<div id="data-paymentvoucher"></div>
<div class="col-md-12">
    <table class="table table-striped table-hover" id="TData">
        <thead>
            <tr>
                <td colspan="7">
                    <label># Details Of Payment Voucher</label>
                </td>
            </tr>
            <tr>
                <th>Invoice Number: Date, Amount, Remark</th>
            </tr>
        </thead>
        <tbody>
            <?php $v = 0; foreach($payment_voucher_detail as $data_detil): ?>
            <tr class="index" id="<?php echo $v;?>">
                <?php foreach($invoices as $c): ?>
                    <?php if($c->id==$data_detil->invoice_id): ?>
                        <td style="text-align:Left;">  <?php echo$c->invoice_number . ', ' . $payment_voucher->date . ', amount =' . number_format($data_detil->amount,0,",",".") . ', ' .  $data_detil->remark;?> </td>
                        <?php Else: ?>
                        <?php EndIf; ?>     
                        <?php EndForeach; ?>
                        <?php $v++; EndForeach; ?>
                    </tbody>
                </table>
                <table>
                    <td style="text-align:right;"> Total Amount : <?php echo number_format($payment_voucher_detail_amount,0,",",".") ?> </td>
                </table>
            </div>
        </form>
