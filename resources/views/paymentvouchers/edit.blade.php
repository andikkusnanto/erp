<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>
    <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}">
    <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
    <script src="{{ asset('js/summernote/summernote.js') }}"</script> 
    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
    <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
    <script src="{{ asset('js/jquery/select2.js') }}"</script>
    <script src="{{ asset('js/jquery/date.js') }}"</script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            ]) !!};
        </script>
</head>
<div class="col-md-12">
    <h4><b>Payment Voucher</b></h4>
    <h2><a href="{{route('paymentvoucher.index')}}" data-toggle="tooltip" title="Back To Grid"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
</div>
<form class="form-horizontal" method="POST" action="{{ route('paymentvoucher.getsave') }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-8">
                <label class="control-label">id</label>
                <input type="text" id="id" value="<?php echo $payment_voucher->id;?>" name="id" class="form-control" readonly="true"/>
            </div>
        </div>   
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-8">
                <label class="control-label">Pv Number</label>
                <input type="text" id="pv_number" value="{{ $payment_voucher->pv_number}}" name="pv_number" class="form-control" required/>
            </div>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-sm-2 control-label" hidden="true">counterx</label>
        <div class="col-sm-6">
            <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-2">
                 <label class="control-label">Date</label>
                 <input type="date" value="{{ $payment_voucher->date }}" id="date" name="date" class="form-control" required/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-8">
             <label class="control-label">External Provider</label>
                <select name="external_provider_id" id="external_provider_id" class="form-control">
                    <option></option>
                    <?php foreach($external_providers as $c): ?>
                        <?php if($c->id==$payment_voucher->external_provider_id): ?>
                                <option value="<?php echo $c->id;?>" selected><?php echo $c->expname;?></option>
                            <?php Else: ?>
                                <option value="<?php echo $c->id;?>"><?php echo $c->expname;?></option>
                            <?php EndIf; ?>     
                        <?php EndForeach; ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-8">
                <label class="control-label">Remark</label>
                <input type="text" id="remark" value="<?php echo $payment_voucher->remark;?>" name="remark" class="form-control"/>
            </div>
        </div>
    </div>
    <div id="data-data"></div>
        <table class="table table-striped table-hover" id="TData">
            <thead>
                <tr>
                    <td colspan="7">
                        <label># Details Of Payment Voucher</label>
                        <h3><a style="color: green" href="javascript:void(0)" onclick="AddPvDetil()" title="Add New data Detil" class="pull-right"><span class="glyphicon glyphicon-plus"></span></a></h3>
                    </td>
                </tr>
                <tr>
                    <th>Invoice Number: Date, Amount</th>
                    <th style="text-align:center;">Remark</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <script type="text/javascript">
                    var idx=0;
                </script>
                <?php $v = 0; foreach($payment_voucher_detail as $data_detail): ?>
                    <tr class="index" id="<?php echo $v;?>">
                        <td><select id="invoice_id<?php echo $v;?>" name="invoice_id<?php echo $v;?>" class="form-control">
                            <option></option>
                            <?php foreach($invoices as $c): ?>
                                <?php if($c->id==$data_detail->invoice_id): ?>
                                    <option value="<?php echo $c->id;?>" selected><?php echo $c->invoice_number . ', ' . $payment_voucher->date . ', amount =' . number_format($data_detail->amount,0,",",".");?></option>
                                    <?php Else: ?>
                                    <option value="<?php echo $c->id;?>"><?php echo $c->invoice_number . ', ' . $payment_voucher->date . ', amount =' . number_format($data_detail->amount,0,",",".");?></option>
                                    <?php EndIf; ?>     
                                    <?php EndForeach; ?>
                                </select></td>
                                <td><input type="text" value="<?php echo $data_detail->remark;?>" id="remark<?php echo $v;?>" name="remark<?php echo $v;?>" class="form-control" /></td>
                                <script type="text/javascript">
                                    var nm = $('#v_jv').val()
                                    $('#invoice_id'+idx).select2({
                                        ajax: {
                                            placeholder: 'Payment Voucher Order Detil',    
                                            url: '<?php echo url('/paymentvouchers/getinvoice');?>',
                                            dataType: 'json',
                                            quietMillis: 100,
                                            data: function (term) {
                                                return {
                                                    q: term, // search term
                                                };
                                            },
                                            results: function (data) {
                                                var myResults = [];
                                                $.each(data, function (index, item) {
                                                    myResults.push({
                                                        'id': item.id,
                                                        'text': item.text
                                                    });
                                                });
                                                return {
                                                    results: myResults
                                                };
                                            },
                                            minimumInputLength: 3
                                        }
                                    });
                                </script>
                                <td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)"><i class="fa fa-trash"></i></a></td>
                                <script type="text/javascript">
                                    idx=idx+1;
                                </script>
                    </tr>
                <?php $v++; EndForeach; ?>
            </tbody>
        </table>
    </div>
    <div class="pull-right">
        <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><i class="fa fa-refresh"></i></button>
        <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i></button>
    </div>
</form>

<script type="text/javascript">
    if (idx<=0) {idx=0} else {}
        $(document).ready(function(){
        });
    function AddPvDetil(){
        var row = $('#TData tbody tr').length;
        if(row>0){
            var index = $('.index').last().attr('id');
            index = parseInt(index);
            row = parseInt(row);
            row = index+1;
        }
        $('#counterx').val(idx);
        document.getElementById("counterx").innerHTML=idx;
        var x = document.getElementById("counterx");
        var html = '<tr id="'+idx+'" class="index">';
        html += '<td><input type="hidden" id="invoice_id'+row+'" name="invoice_id'+idx+'" class="form-control" required/></td>';
        html += '<td><input type="text" id="remark'+row+'" name="remark'+idx+'" class="form-control" /></td>';
        html += '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>';
        html += '</tr>';
        $('#TData tbody').append(html);
        idx=idx+1;
        $('#counterx').val(idx);
        $('#invoice_id'+row).select2({
            ajax: {
                placeholder: 'Invoice Number',    
                url: '<?php echo url('/paymentvouchers/getinvoice');?>',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text + ', ' + item.text1
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });
    }

    function deleteRow(btn) {
      var row = btn.parentNode.parentNode;
      row.parentNode.removeChild(row);
      idx=idx-1;
      if (idx<=0) {idx=0} else {}
          $('#counterx').val(idx);
    }
  
    $('#counterx').val(idx);

</script>
