@if (count($errors) > 0)

<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>

@endif

{!! Form::model($data, ['method' => 'POST','route' => ['appmenu.getsave', $data->id]]) !!}
<div class="col-md-12">
    <div class="col-md-4 col-xs-4 konten">
        <h4><b>Edit New Item</b></h4>
        <h2><a href="{{ route('appmenu.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
        <div class="form-group">
            <strong>id:</strong>
            {!! Form::text('id', null, array('placeholder' => 'id','class' => 'form-control','readonly')) !!}
        </div>

        <div class="form-group">
            <strong>Name</strong>
            {!! Form::text('name', null, array('placeholder' => 'name','class' => 'form-control')) !!}
        </div>

        <div class="form-group">
            <strong>Href</strong>
            {!! Form::text('href', null, array('placeholder' => 'href','class' => 'form-control')) !!}
        </div>

        <div class="form-group">
            <strong>Description:</strong>
            {!! Form::text('description', null, array('placeholder' => 'description','class' => 'form-control')) !!}
        </div>
        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Submit </button>
    </div>
</div>

{!! Form::close() !!}


