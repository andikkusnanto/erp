@if (count($errors) > 0)

<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.<br><br>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>

@endif
<div class="col-md-12">
	<div class="col-md-4">
		<h4><b>Create New Item</b></h4>
		<h2><a href="{{ route('appmenu.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
		{!! Form::open(array('route' => 'appmenu.getsave','method'=>'POST')) !!}

		<strong>Name:</strong>
		{!! Form::text('name', null, array('placeholder' => 'name', 'class' => 'form-control')) !!}

		<strong>Href:</strong>
		{!! Form::text('href', null, array('placeholder' => 'href', 'class' => 'form-control')) !!}

		<strong>Description:</strong>
		{!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control')) !!}
		<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Submit </button>
		{!! Form::close() !!}
	</div>
</div>
