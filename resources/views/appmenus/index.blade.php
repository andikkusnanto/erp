<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>

<div class="col-md-12"> 
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
</div>
@endif
<div class="col-md-12"> 
    <h4><b>App Menus</b></h4>
    <table class="table table-striped table-hover" id="tblappmenu">
    <thead>
        <tr>
            <tr>
                <th colspan="6">
                    <h3 class="pull-right">
                        <a style="color: green" href="{{ route('appmenu.create') }}">
                            <span class="glyphicon glyphicon-plus"></span>
                        </a>
                    </h3>
                </th>
            </tr>
            <th >No</th>
            <th>Menu</th>
            <th>Href</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $key => $datas)
        <tr>
            <td style="text-align: center">{{ $key+1 }}</td>
            <td>{{ $datas->name }}</td>
            <td>{{ $datas->href }}</td>
            <td>{{ $datas->description }}</td>
            <td>
                <a href="{{ route('appmenu.edit',$datas->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{{ route('appmenu.show',$datas->id) }}"><i class="glyphicon glyphicon-search"></i></a>
                <a style="color:red" href="{{ route('appmenu.destroy',$datas->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLappmenu').DataTable();
    } );
</script>
