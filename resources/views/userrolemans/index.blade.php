<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>
        <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
            <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}">
            <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
                <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
                <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
                <script src="{{ asset('js/summernote/summernote.js') }}"</script> 
                    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
                        <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
                            <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 
                            <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
                            <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
                            <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
                            <script src="{{ asset('js/jquery/select2.js') }}"</script>
                                <script src="{{ asset('js/jquery/date.js') }}"</script>
                                    <script>
                                        window.Laravel = {!! json_encode([
                                            'csrfToken' => csrf_token(),
                                            ]) !!};
                                        </script>
                                    </head>

                                    <div class="col-md-12">
                                        <h4><b>User Management</b></h4>
                                        <h2><a href="{{route('userroleman.index')}}" data-toggle="tooltip" title="Back To Grid"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
                                    </div>

                                    <form class="form-horizontal" method="POST" action="{{ route('userroleman.getsave') }}">

                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" hidden="true">counterx</label>
                                            <div class="col-sm-6">
                                                <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
                                            </div>
                                        </div>

                                            <div id="data-data"></div>
                                            <table class="table table-striped table-hover" id="TData">
                                                <thead>
                                                    <tr>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <th>User Account / e-Mail Account</th>
                                                        <th>Full Name</th>
                                                        <th>Role</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <script type="text/javascript">
                                                        var idx=0;

                                                    </script>

                                                    <?php $v = 0; foreach($users as $data): ?>

                                                    <tr class="index" id="<?php echo $v;?>">
                                                        <td><input type="text" style="text-align:left;" value="<?php echo $data->email;?>" id="email<?php echo $v;?>" name="email<?php echo $v;?>" class="form-control" readonly="true" /></td>

                                                        <td><input type="text" style="text-align:left;" value="<?php echo $data->name;?>" id="name<?php echo $v;?>" name="name<?php echo $v;?>" class="form-control" readonly="true" /></td>

                                                        <td><select id="user_role_id<?php echo $v;?>" name="user_role_id<?php echo $v;?>" class="form-control">
                                                            <option></option>
                                                            <?php foreach($user_roles as $c): ?>
                                                                <?php if($c->id==$data->user_role_id): ?>
                                                                    <option value="<?php echo $c->id;?>" selected><?php echo $c->role;?></option>
                                                                    <?php Else: ?>
                                                                    <option value="<?php echo $c->id;?>"><?php echo $c->role;?></option>
                                                                    <?php EndIf; ?>     
                                                                    <?php EndForeach; ?>
                                                                </select></td>

                                    <script type="text/javascript">
                                        idx=idx+1;
                                    </script>

                                </tr>
                                <?php $v++; EndForeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><i class="fa fa-refresh"></i></button>
                        <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i></button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    if (idx<=0) {idx=0} else {}

        $(document).ready(function(){

        });

    function deleteRow(btn) {
      
      var row = btn.parentNode.parentNode;
      var email = $('#email'+row).val();
      row.parentNode.removeChild(row);
      idx=idx-1;
      if (idx<=0) {idx=0} else {}

          $('#counterx').val(idx);
  }

  $('#counterx').val(idx);

    </script>
