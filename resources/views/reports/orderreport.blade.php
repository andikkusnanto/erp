<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>

<div class="col-md-12">
    <h4><b>Financial Report</b></h4>
</div>

<br>
<?php 
$no = 1; 
if(isset($_GET['page'])){
    $no = ($_GET['page']*10)-9;
}
?>
@if (Session::has('message'))

<div class="alert alert-success">
    <i class="fa fa-exclamation-circle"></i><small>  {{Session::get('message')}} !!</small>
    <button type="button" class="close" data-dismiss="alert">
        ×
    </button>
</div>
@endif
<div class="col-md-12">
    <table class="table table-striped table-hover" id="TBLorderreport">
        <thead>
            <tr>
                <th style="text-align:left;">#</th>
                <th style="text-align:left;">Purchase Order</th>
                <th style="text-align:left;">Delivery Order</th>
                <th style="text-align:left;">Invoice</th>
                <th style="text-align:left;">Payment Voucher</th>
            </tr>
        </thead>
        <tbody>
            <!-- <?php $i=0; ?> -->
            <?php foreach($data as $row): ?>

                <tr>
                    <!-- {{++$i}}   -->
                    <td><?php echo $no;?></td>
                    <td><?php echo $row->po_number . ': ' . $row->po_date . ', ' . $row->product_name . ', ' . number_format($row->po_amount,0,",",".");?></td>
                    <td><?php echo $row->do_number . ': ' . $row->do_date . ', ' . $row->do_remark;?></td>
                    <td><?php echo $row->invoice_number . ': ' . $row->invoice_date . ', ' . number_format($row->invoice_amount,0,",",".");?></td>
                    <td><?php echo $row->pv_number . ': ' . $row->pv_date . ', ' . $row->pv_remark;?></td>
                </tr>
                <?php $no++; EndForeach; ?>
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#TBLorderreport').DataTable();
        } );
    </script>







