<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>

        <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
            <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}"</script>
            <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
                <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
                <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
                <script src="{{ asset('js/summernote/summernote.js') }}"</script> 


                    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
                        <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
                            <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 

                            <!-- hidden -->
                            <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">


                            <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
                            <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
                            <script src="{{ asset('js/jquery/select2.js') }}"</script>

                                <!-- hidden -->
                                <script src="{{ asset('js/jquery/date.js') }}"</script>
                                    <!-- <script src="{{ asset('js/common.js') }}" </script> -->

                                    <script>
                                        window.Laravel = {!! json_encode([
                                            'csrfToken' => csrf_token(),
                                            ]) !!};
                                        </script>

                                    </head>

                                    <div class="col-md-12">
                                       <h4><b>Delivery Order</b></h4>
                                       <h3><a href="{{route('deliveryorder.index')}}" data-toggle="tooltip" title="Back To Grid" ><span class="glyphicon glyphicon-arrow-left"></span></a></h3>
                                    </div>

                                    <form class="form-horizontal" method="POST" action="{{ route('deliveryorder.getsave','4') }}">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                        <div class="form-group required">
                                            <label class="control-label" hidden="true">counterx</label>
                                            <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group required">
                                                <div class="col-md-5">
                                                    <label class="control-label">ID</label>
                                                    <input type="text" id="id" value="<?php echo $delivery_order->id;?>" name="id" class="form-control" readonly="true"/>
                                                </div>
                                                <div class="required">
                                                    <div class="col-md-5">
                                                        <label class="control-label">DO Number</label>
                                                        <input type="text" id="do_number" value="<?php echo $delivery_order->do_number;?>" name="do_number" class="form-control" required/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                 <label class="control-label">Date</label>
                                                 <input type="date" id="date" value="<?php echo $delivery_order->date;?>" name="date" class="form-control" required/>
                                             </div>

                                             <div class="required">
                                                 <div class="col-md-5">
                                                    <label class="control-label">External Provider</label>
                                                    <select name="external_provider_id" id="external_provider_id" class="form-control">
                                                        <option></option>
                                                        <?php foreach($external_providers as $c): ?>
                                                            <?php if($c->id==$delivery_order->external_provider_id): ?>
                                                                <option value="<?php echo $c->id;?>" selected><?php echo $c->expname;?></option>
                                                                <?php Else: ?>
                                                                <option value="<?php echo $c->id;?>"><?php echo $c->expname;?></option>
                                                                <?php EndIf; ?>     
                                                                <?php EndForeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group required">
                                                    <div class="col-md-5">
                                                        <label class="control-label">Delivery Address</label>
                                                        <input type="text" id="ship_to" value="<?php echo $delivery_order->ship_to;?>" name="ship_to" class="form-control"/>
                                                    </div>
                                                    <div class="required">
                                                        <div class="col-md-5">
                                                            <label class="control-label">Receiver</label>
                                                            <input type="text" id="receiver" value="<?php echo $delivery_order->receiver;?>" name="receiver" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group required">
                                                    <div class="col-md-5">
                                                        <label class="control-label">Approval</label>
                                                        <input type="text" id="approval" value="<?php echo $delivery_order->approval;?>" name="approval" class="form-control"/>
                                                    </div>

                                                    <div class="required">
                                                        <div class="col-md-5">
                                                            <label class="control-label" id="demo">Remark</label>
                                                            <input type="text" value="<?php echo $delivery_order->remark;?>" id="remark" name="remark" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="data-deliveryorder"></div>

                                            <table class="table table-striped table-hover" id="TData">
                                                <thead>
                                                    <tr>
                                                        <td colspan="7">
                                                            <label># Details Of Delivery Order</label>
                                                            <h3><a href="javascript:void(0)" onclick="AddDoDetil()" title="Add New Delivery Order Detil" style="color: green ">
                                                                <span class="glyphicon glyphicon-plus pull-right"></span>
                                                            </a></h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>PO Number: Product Service, Qty</th>
                                                        <th style="text-align:center;">Qty</th>
                                                        <th style="text-align:center;">Remark</th>
                                                        <th style="text-align:center;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <script type="text/javascript">
                                                        var idx=0;
                                                    </script>

                                                    <?php $v = 0; foreach($delivery_order_detail as $data_detail): ?>

                                                    <tr class="index" id="<?php echo $v;?>">

                                                        <td><select id="purchase_order_detil_id<?php echo $v;?>" name="purchase_order_detil_id<?php echo $v;?>" class="form-control">
                                                            <option></option>
                                                            <?php foreach($purchase_order_details as $c): ?>
                                                                <?php if($c->id==$data_detail->purchase_order_detail_id): ?>
                                                                    <option value="<?php echo $c->id;?>" selected><?php echo $c->po_number . ': ' . $c->productService->product_name . ', ' . $c->qty . ' ' . $c->uom->name;?></option>
                                                                    <?php Else: ?>
                                                                    <option value="<?php echo $c->id;?>"><?php echo $c->po_number . ': ' . $c->productService->Product_name . ', ' . $c->qty . ' ' . $c->uom->name;?></option>
                                                                    <?php EndIf; ?>     
                                                                    <?php EndForeach; ?>
                                                                </select></td>

                                                                <td><input type="text" value="<?php echo $data_detail->qty;?>" id="qty<?php echo $v;?>" name="qty<?php echo $v;?>" class="form-control" /></td>

                                                                <td><input type="text" value="<?php echo $data_detail->remark;?>" id="remark<?php echo $v;?>" name="remark<?php echo $v;?>" class="form-control" /></td>

                                                                <script type="text/javascript">
                                                                    var nm = $('#v_jv').val()

                                        // $('#reff').val(idx).val();

                                        $('#purchase_order_detil_id'+idx).select2({

                                            ajax: {

                                                placeholder: 'Purchase Order Detil',    

                                                url: '<?php echo url('/deliveryorders/getpurchaseorder');?>',

                                                dataType: 'json',
                                                quietMillis: 100,
                                                data: function (term) {
                                                    return {
                                                        q: term, // search term
                                                    };
                                                },
                                                results: function (data) {
                                                    var myResults = [];
                                                    $.each(data, function (index, item) {
                                                        myResults.push({
                                                            'id': item.id,
                                                            'text': item.text
                                                        });
                                                    });
                                                    return {
                                                        results: myResults
                                                    };
                                                },
                                                minimumInputLength: 3
                                            }

                                        });

                                    </script>

                                    <td><a style="color: red" href="javascript:void(0)" onclick="javascript:deleteRow(this)"><i class="glyphicon glyphicon-trash"></i></a></td>

                                    <script type="text/javascript">
                                        idx=idx+1;
                                    </script>

                                </tr>
                                <?php $v++; EndForeach; ?>

                            </tbody>
                        </table>

                        <div class="pull-right">
                            <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><span class="glyphicon glyphicon-refresh"></span></button>
                            <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span></button>

                        </div>
                    </form>           

                    <script type="text/javascript">
                        if (idx<=0) {idx=0} else {}
                            $(document).ready(function(){
                            });

                        function AddDoDetil(){
                            var row = $('#TData tbody tr').length;
                            if(row>0){
                                var index = $('.index').last().attr('id');
                                index = parseInt(index);
                                row = parseInt(row);
                                row = index+1;
                            }

                            $('#counterx').val(idx);

                            document.getElementById("counterx").innerHTML=idx;
                            var x = document.getElementById("counterx");

                            var html = '<tr id="'+idx+'" class="index">';
                            html += '<td><input type="hidden" id="purchase_order_detil_id'+row+'" name="purchase_order_detil_id'+idx+'" class="form-control" required/></td>';
                            html += '<td><input type="text" id="qty'+row+'" name="qty'+idx+'" class="form-control" /></td>';
                            html += '<td><input type="text" id="remark'+row+'" name="remark'+idx+'" class="form-control" /></td>';
                            html += '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="glyphicon glyphicon-trash"><i class="fa fa-times"></i></a></td>';
                            html += '</tr>';

                            $('#TData tbody').append(html);
                            idx=idx+1;
                            $('#purchase_order_detil_id'+row).select2({

                                ajax: {
                                    placeholder: 'PO Number',    
                                    url: '<?php echo url('/deliveryorders/getpurchaseorder');?>',
                                    dataType: 'json',
                                    quietMillis: 100,
                                    data: function (term) {
                                        return {
                                        q: term, // search term
                                    };
                                },
                                results: function (data) {
                                    var myResults = [];
                                    $.each(data, function (index, item) {
                                        myResults.push({
                                            'id': item.id,
                                            'text': item.text + ' ' + item.text1       
                                        });

                                    });
                                    return {
                                        results: myResults   
                                    };
                                },
                                minimumInputLength: 3
                            }
                        });
                        }

                        function deleteRow(btn) {
                          var row = btn.parentNode.parentNode;
                          row.parentNode.removeChild(row);
                          idx=idx-1;
                          if (idx<=0) {idx=0} else {}

                              $('#counterx').val(idx);
                      }

                      $('#counterx').val(idx);
                  </script>
