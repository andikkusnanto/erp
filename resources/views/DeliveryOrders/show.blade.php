<div class="col-md-12">
    <h4><b>Delivery Order</b></h4>
    <h3><a href="{{route('deliveryorder.index')}}" data-toggle="tooltip" title="Back To Grid" ><span class="glyphicon glyphicon-arrow-left"></span></a></h3>
</div>

<form class="form-horizontal" method="POST" action="{{ route('deliveryorder.getsave','4') }}">

    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="form-group required">
        <label class="col-sm-2 control-label" hidden="true">counterx</label>
        <div class="col-sm-6">
            <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-sm-5">
                <label class="control-label">DO Number</label>
                <input type="label" id="do_number" value="<?php echo $delivery_order->do_number;?>" name="do_number" class="form-control" readonly ="true"/>
            </div>

            <div class="required">
             <div class="col-md-5">
                <label class="control-label">Date</label>
                <input type="label" id="date" value="<?php echo $delivery_order->date;?>" name="date" class="form-control" readonly ="true" />
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="control-label">External Provider</label>
            <input type="text" id="external_provider_id" value="<?php echo  $delivery_order->externalprovider->expname;?>" name="external_provider_id" class="form-control"  readonly ="true"/>
        </div>

        <div class="col-md-5">
         <label class="control-label">Delivery Address</label>
         <input type="text"  value="<?php echo $delivery_order->ship_to;?>" id="shipto" name="ship_to" class="form-control" readonly ="true"/>
     </div>
 </div>
</div>

<div class="col-md-12">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="control-label">Receiver</label>    
            <input type="text" id="receiver" value="<?php echo $delivery_order->receiver;?>" name="receiver" class="form-control" readonly ="true"/>
        </div>

        <div class="required">
            <div class="col-md-5">
                <label class="control-label">Approval</label>
                <input type="text" id="approval" value="<?php echo $delivery_order->approval;?>" name="approval" class="form-control" readonly ="true"/>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group required">
        <div class="col-md-10">
            <label class="control-label">Remark</label>    
            <input type="text" id="remark" value="<?php echo $delivery_order->remark;?>" name="remark" class="form-control" readonly ="true"/>
        </div>
    </div>
</div>

<div id="data-deliveryorder"></div>
<table class="table table-bordered table-hover" id="TData">
    <thead>
        <tr>
            <td colspan="7">
                <label># Details Of Delivery Order</label>
            </td>
        </tr>
        <tr>
            <th>PO Number: Product Service, Qty</th>
            <th style="text-align:center;">Remark</th>
        </tr>
    </thead>
    <tbody>
        <?php $v = 0; foreach($delivery_order->deliveryOrderDetail as $delivery_order_detail): ?>
        <tr class="index" id="<?php echo $v;?>">

            <td style="text-align:Left;"> <?php echo $delivery_order_detail->po_number . ': ' . $delivery_order_detail->productservice->product_name . ', ' . $delivery_order_detail->qty .' ' . $delivery_order_detail->uom->name ;?> </td>

            <td style="text-align:center;">  <?php echo $delivery_order_detail->remark;?> </td>
        </tr>
        <?php $v++; EndForeach; ?>

    </tbody>
</table>
</form>