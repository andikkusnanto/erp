<div class="col-md-12">
    <h4><b>Invoices</b></h4>
    <h3><a href="{{route('invoice.index')}}" data-toggle="tooltip" title="Back To Grid" ><span class="glyphicon glyphicon-arrow-left"></span></a></h3>
</div>
<form class="form-horizontal" method="POST" action="{{ route('invoice.getsave','4') }}">

    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="form-group required">
        <label class="col-sm-2 control-label" hidden="true">counterx</label>
        <div class="col-sm-6">
            <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-5">
                <label class="control-label">Invoice Number</label>
                <input type="text" id="invoice_number" value="<?php echo $invoice->invoice_number;?>" name="invoice_number" class="form-control" readonly="true"/>
            </div>

            <div class="form-group">
                <div class="col-md-5">
                 <label class="control-label">Date</label>
                 <input type="date" value="<?php echo $invoice->date;?>" id="date" name="date" class="form-control" readonly="true"/>
             </div>
         </div>
     </div>
 </div>

 <div class="col-md-12">
     <div class="form-group required">
         <div class="col-md-5">
         <label class="control-label">External Provider</label>
             <input type="text" id="external_provider_id" value="<?php echo $invoice->externalprovider->expname;?>" name="external_provider_id" class="form-control"  readonly="true"/>
         </div>

         <div class="form-group required">
             <div class="col-md-5">
                <label class="control-label">Remark</label>
                <input type="text" id="remark" value="<?php echo $invoice->remark;?>" name="remark" class="form-control" readonly="true"/>
            </div>
        </div>
    </div>
</div>

<div id="data-invoice"></div>
<table class="table table-bordered table-hover" id="TData">

    <tr>
        <td colspan="7">
            <label># Details Of Invoice</label>
        </td>
    </tr>
    <tr>
        <th>DO Number: Product Service, Qty</th>
        <th style="text-align:center;">Unit Price</th>
        <th style="text-align:center;">Amount</th>
        <th style="text-align:center;">Remark</th>
    </tr>

    <?php $v = 0; foreach($invoice->invoiceDetail as $invoice_detail): ?>
    <tr class="index" id="<?php echo $v;?>">
        <td style="text-align:Left;"> <?php echo $invoice_detail->do_number .' : '.  $invoice_detail->deliveryOrderDetail->productservice->product_name .', ' . $invoice_detail->qty .' ' . $invoice_detail->uom->name ;?> </td>
        <td style="text-align:right;">  <?php echo number_format($invoice_detail->unit_price,0,",",".") ?> </td>
        <td style="text-align:right;">  <?php echo number_format($invoice_detail->amount,0,",",".") ?> </td>
        <td style="text-align:right;">  <?php echo $invoice_detail->remark;?> </td>
    </tr>
    <?php $v++; EndForeach; ?>

</form>
<table>
    <td style="text-align:right;"> Total Amount : <?php echo number_format($invoice_detail_amount,0,",",".") ?> </td>
</table>


