<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>
    <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}"</script>
    <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
    <script src="{{ asset('js/summernote/summernote.js') }}"</script> 
    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
    <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
    <script src="{{ asset('js/jquery/select2.js') }}"</script>
    <script src="{{ asset('js/jquery/date.js') }}"</script>
    <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<div class="col-lg-12">
    <div class="col-md-12"> 
        <h4><b>Invoices</b></h4> 
        <h3><a href="{{ route('invoice.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h3>
    </div>
</div>

<form class="form-horizontal" method="POST" action="{{ route('invoice.getsave','4') }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="form-group required">
        <label class="col-sm-2 control-label" hidden="true">counterx</label>
        <div class="col-sm-6">
            <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-5">
                <label class="control-label">Invoice Number</label>  
                <input type="text" id="invoice_number" name="invoice_number" class="form-control" required/>
            </div>
            <div class="form-group">
                <div class="col-md-5">
                   <label class="control-label">Date</label>
                   <input type="date"  value="<?php echo date('Y-m-d');?>" id="date" name="date" class="form-control" required/>
               </div>
           </div>
       </div>
   </div>
   <div class="col-md-12">
       <div class="form-group required">
           <div class="col-sm-5">
               <label class="control-label">External Provider</label>
               <input type="hidden" id="external_provider_id" name="external_provider_id" class="form-control"  required/>
           </div>
           <div class="form-group required">
               <div class="col-md-5">
                <label class="control-label">Remark</label>
                <input type="text" id="remark" name="remark" class="form-control"/>
            </div>
        </div>
    </div>
</div>
<div id="data-invoice"></div>
<table class="table table-bordered table-hover" id="TData">
    <thead>
        <tr>
            <td colspan="7">
                <label># Details Of Invoice</label>
                <h3 class="pull-right"><a style="color: green" href="javascript:void(0)" onclick="AddInvDetil()" title="Add New Invoice Detil"><span class="glyphicon glyphicon-plus"></span></a></h3>
            </td>
        </tr>
        <tr>
            <th>DO Number: Product Service, Qty</th>
            <th style="text-align:center;">Unit Price</th>
            <th style="text-align:center;">Amount</th>
            <th style="text-align:center;">Remark</th>
            <th style="text-align:center;">Action</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<div class="pull-right">
    <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><i class="fa fa-refresh"></i></button>
    <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i></button>
</div>
</form>
<script type="text/javascript">
    var idx=0;
    if (idx<=0) {idx=0} else {}

        $(document).ready(function(){

            $('#external_provider_id').select2({

                ajax: {
                    placeholder: 'External Provider',    
                    url: '<?php echo url('/invoices/getexternalprovider');?>',
                    dataType: 'json',
                    quietMillis: 100,
                    data: function (term) {
                        return {
                            q: term, // search term
                            };
                    },

                    results: function (data) {
                        var myResults = [];
                        $.each(data, function (index, item) {
                            myResults.push({
                                'id': item.id,
                                'text': item.text
                            });
                        });
                        return {
                            results: myResults
                        };
                    },
                    minimumInputLength: 3
                }
            });

        });

    function AddInvDetil(){
        var row = $('#TData tbody tr').length;
        if(row>0){
            var index = $('.index').last().attr('id');
            index = parseInt(index);
            row = parseInt(row);
            row = index+1;
            idx=index+1;
        }

        $('#counterx').val(idx);

        document.getElementById("counterx").innerHTML=idx;
        var x = document.getElementById("counterx");
        var html = '<tr id="'+idx+'" class="index">';
        html += '<td><input type="hidden" id="delivery_order_detil_id'+row+'" name="delivery_order_detil_id'+idx+'" class="form-control" required/></td>';
        html += '<td><input type="text" id="unit_price'+row+'" name="unit_price'+idx+'" class="form-control" readonly="true" /></td>';
        html += '<td><input type="text" id="amount'+row+'" name="amount'+idx+'" class="form-control" readonly="true" /></td>';
        html += '<td><input type="text" id="remark'+row+'" name="remark'+idx+'" class="form-control" /></td>';
        html += '<td><a style="color:red" href="javascript:void(0)" onclick="javascript:deleteRow(this)"<span class="glyphicon glyphicon-trash"></span></a></td>';
        html += '</tr>';

        $('#TData tbody').append(html);
        $('#delivery_order_detil_id'+row).select2({

            ajax: {

                placeholder: 'DO Number',    

                url: '<?php echo url('/invoices/getdeliveryorder');?>',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text + ': ' + item.text1
                        });
                    });
                    return {
                        results: myResults
                    };
                },
            minimumInputLength: 3
            }
        });
    }

    function fuom(x){
        var id = $(x).attr('id');
        var value = $('#'+id).val();
        var uom = LoadUom(value);
        $('#uom_id'+id).html(uom);
        $('#uom_id'+id).select2();
    }

    function LoadUom(id){
        var value = '';
        $.ajax({
            url: '<?php echo url('/deliveryorders/getuom');?>',
            dataType: 'html',
            async: false,
            success:function(data){
                value = data;
            }
        });
        return value;
    }

    function DeleteService(x){
        $('#service'+x).remove();
    }

    function deleteRow(btn) {
        var row = btn.parentNode.parentNode;
        row.parentNode.removeChild(row);
        idx=idx-1;
        if (idx<=0) {idx=0} else {}
            $('#counterx').val(idx);
    }

    function famount(idx) {
        var prmqty = 0;
        var prmunit_price = 0;
        var prmamount=0;
        prmqty=$('#qty'+idx).val();
        prmunit_price=$('#unit_price'+idx).val();
        prmamount=prmqty * prmunit_price;
        $('#amount'+idx).val(prmamount);
    }

    AddInvDetil();

</script>