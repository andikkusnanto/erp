<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>

<div class="col-lg-12">
    <h4><b>Invoice List</b></h4>
    <form method="GET" href="{{route('invoice.index')}}">
        <div class="col-md-9">
            <div class="col-md-3">
                <label>First Date</label>
                <input type="date" name="date_first" placeholder="First Date" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
            </div>
            <div class="col-md-3">
                <label>Last Date</label> 
                <div class="input-group">    
                    <input type="date" name="date_last" placeholder="Last Date" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">  
                    <span class="input-group-btn">         
                        <button type="submit" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>  
        </div>
    </form>
</div>
<?php 
$no = 1; 
if(isset($_GET['page'])){
    $no = ($_GET['page']*10)-9;
}
?>
<?php if (Session::has('message')): ?>
    <div class="alert alert-success">
        <i class="fa fa-exclamation-circle"></i><small>  <?php echo Session::get('message'); ?> !!</small>
        <button type="button" class="close" data-dismiss="alert">
            ×
        </button>
    </div>
<?php endif; ?>

<table class="table striped table-hover" id="TBLinvoice">
<thead>
    <tr>
        <tr>
            <th colspan="7">
                <h3 class="pull-right">
                    <a style="color: green" href="{{ route('invoice.create') }}">
                        <span class="glyphicon glyphicon-plus" class="pull-right"></span>
                    </a>
                </h3>
            </th>
        </tr>
        <th>#</th>
        <th>Invoice Number</th>
        <th>Date</th>
        <th>External Provider</th>
        <th>Remark</th>
        <th>Total Amount</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    <?php $i=0; ?>
    <?php foreach($data as $row): ?>
        <script type="text/javascript">
            function gettotals(idx, x)
            {
                var x = x+1;
                var invoice_id = idx;

                $.ajax({
                    type:'get',
                    url: '<?php echo url('/invoices/totals');?>',
                    data:{'id':invoice_id},
                    datatype:'json',
                    success:function(data){
                        var s = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        document.getElementById("grandtotal"+x).innerHTML = s;
                    },
                    error:function(){
                    }
                });
            }
        </script>

        <script type="text/javascript"> 
            gettotals({{$row->id}}, {{ $i }}); 

        </script>

        <tr>
            <!-- {{++$i}}   -->
            <td><?php echo $no;?></td>
            <td><?php echo $row->invoice_number;?></td>
            <td><?php echo $row->date;?></td>
            <td><?php echo $row->externalprovider->expname;?></td>
            <td><?php echo $row->remark;?></td>
            <td style="text-align:left;"><text id ='grandtotal{{ $i }}' > </text> </td>
            <td>
                <a href="{{ route('invoice.edit',$row->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{{ route('invoice.show',$row->id) }}"><i class="glyphicon glyphicon-search"></i></a>
                <a style="color: red" href="{{ route('invoice.destroy',$row->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
        <?php $no++;  EndForeach; ?>
</tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLinvoice').DataTable();
    });
</script>


