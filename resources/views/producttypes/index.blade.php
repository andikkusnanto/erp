<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>

<div class="col-md-12"> 
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
</div>
@endif
<div class="col-md-12"> 
    <h4><b>Product Types</b></h4>
    <table class="table table-striped table-hover" id="TBLproducttype">
    <thead>
        <tr>
            <tr>
                <th colspan="6">
                    <h3 class="pull-right">
                        <a style="color: green" href="{{ route('producttype.create') }}">
                            <span class="glyphicon glyphicon-plus"></span>
                        </a>
                    </h3>
                </th>
            </tr>
            <th >No</th>
            <th>Type Name</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $key => $datas)
        <tr>
            <td style="text-align: center">{{ $key+1 }}</td>
            <td>{{ $datas->type_name }}</td>
            <td>{{ $datas->description }}</td>
            <td>
                <a href="{{ route('producttype.edit',$datas->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{{ route('producttype.show',$datas->id) }}"><i class="glyphicon glyphicon-search"></i></a>
                <a style="color:red" href="{{ route('producttype.destroy',$datas->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLproducttype').DataTable();
    } );
</script>
