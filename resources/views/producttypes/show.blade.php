<div class="col-md-12">
	<div class="col-md-5">
		<h4><b>Show List Product Types</b></h4>
		<h2><a href="{{ route('producttype.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
		<div class="form-group">
			<strong>Id :</strong>
			{{ $data->id }}
		</div>
		<div class="form-group">
			<strong>Type Name :</strong>
			{{$data->type_name}}
		</div>
		<div class="form-group">
			<strong>Description :</strong>
			{{ $data->description }}
		</div>
	</div>
</div>