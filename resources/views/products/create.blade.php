<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>
    <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}"</script>
    <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
    <script src="{{ asset('js/summernote/summernote.js') }}"</script> 
    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
    <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
    <script src="{{ asset('js/jquery/select2.js') }}"</script>
    <script src="{{ asset('js/jquery/date.js') }}"</script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            ]) !!};
    </script>
</head>
@if (count($errors) > 0)
    <div class="col-md-12">
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
<div class="col-md-12">
    <h4><b>Product Services</b></h4>
    <h2><a href="{{ route('product.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
</div>
<form class="form-horizontal" method="POST" action="{{ route('product.getupdate','4') }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-8">
                <label class="control-label">Product Type</label>
                <input type="hidden" id="product_type_id" name="product_type_id" class="form-control"  required/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-sm-8">
                <label class="control-label">Product Code</label>
                <input type="text" id="product_code" name="product_code" class="form-control" required/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-sm-8">
                <label class="control-label">Product Name</label>
                <input type="text" id="product_name" name="product_name" class="form-control"/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-sm-8">
                <label class="control-label">Spesification</label>
                <input type="text" id="spesification" name="spesification" class="form-control"/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Submit </button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('#product_type_id').select2({
            ajax: {
                placeholder: 'Product Type',    
                url: '<?php echo url('/products/getproducttype');?>',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });
    });
</script>
