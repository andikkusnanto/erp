<div class="col-md-4">
    <h4><b> Show Item</b></h4>
    <h2><a href="{{ route('product.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
    <div class="form-group">   
        <strong>Id :</strong>
        {{ $data->id }}
    </div>
    <div class="form-group">
        <strong>Product Type :</strong>
        {{ $data->producttype->type_name }}
    </div>
    <div class="form-group">   
        <strong>Product Service Code :</strong>
        {{ $data->product_code }}
    </div>
    <div class="form-group">
        <strong>Product Service :</strong>
        {{ $data->product_name }}
    </div>
    <div class="form-group">
        <strong>Spesification :</strong>
        {{ $data->spesification }}
    </div>
</div>