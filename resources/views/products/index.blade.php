<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>

@if ($message = Session::get('success'))
<div class="col-md-12">
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
</div>
@endif

<div class="col-lg-12">
    <div class="form-group pull-right col-lg-4">
        <form action="{{ route('product.search') }}" method="GET">
            <div class="input-group">
                <input type="text" name="search" placeholder="Search by Product Code, Name, Spesification" id="search" class="form-control">
                <span class="input-group-btn">
                    <button type="submit" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
    </div>
</div>

<div class="col-md-12">
    <h4><b>Product and Services</b></h4>
    <table class="table table-striped table-hover" id="TBLproduct">
        <thead>
            <tr>
                <th colspan="6">
                    <h3 class="pull-right">
                        <a style="color: green" href="{{ route('product.create') }}">
                            <span class="glyphicon glyphicon-plus"></span>
                        </a>
                    </h3>
                </th>
                <tr>
                    <th>No</th>
                    <th>Product Type</th>
                    <th>Product Service Code</th>
                    <th>Product Service</th>
                    <th>Spesification</th>
                    <th>Action</th>
                </tr>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $key => $items)
            <tr>
                <td align="center">{{ $key+1 }}</td>
                <td>{{ $items->producttype->type_name }}</td>
                <td>{{ $items->product_code }}</td>
                <td>{{ $items->product_name }}</td>
                <td>{{ $items->spesification }}</td>
                <td>
                    <a href="{{ route('product.edit',$items->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{{ route('product.show',$items->id) }}"><i class="glyphicon glyphicon-search"></i></a>
                    <a style="color: red" href="{{ route('product.destroy',$items->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLproduct').DataTable();
    } );
</script>


