@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<strong>Whoops!</strong> There were some problems with your input.<br><br>
    	<ul>
    		@foreach ($errors->all() as $error)
    		<li>{{ $error }}</li>
    		@endforeach
    	</ul>
    </div>
@endif

<div class="col-md-12">
    <h4><b>Purchase Order</b></h4>
    <h2><a href="{{ route('product.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h2>
</div>
<div class="col-md-12">
    <form class="form-horizontal" method="POST" action="{{ route('product.getupdate',$product_service->id) }}">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group required">
            <div class="col-md-7">
                <label class="control-label">id</label>
                <input type="text" id="id" value="<?php echo $product_service->id;?>" name="id" class="form-control" readonly="true"/>
            </div>
        </div>                        
        <div class="form-group required">
            <div class="col-md-7">
                <label class="control-label">Product Type</label>
                <select name="product_type_id" id="product_type_id" class="form-control">
                    <option></option>
                    <?php foreach($product_types as $c): ?>
                        <?php if($c->id==$product_service->product_type_id): ?>
                                <option value="<?php echo $c->id;?>" selected><?php echo $c->type_name;?></option>
                            <?php Else: ?>
                                <option value="<?php echo $c->id;?>"><?php echo $c->type_name;?></option>
                            <?php EndIf; ?>     
                            <?php EndForeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <div class="col-md-7">
                        <label class="control-label">Product Code</label>
                        <input type="text" id="product_code" value="<?php echo $product_service->product_code;?>" name="product_code" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group required">
                    <div class="col-md-7">
                        <label class="control-label">Product Service</label>
                        <input type="text" id="product_name" value="<?php echo $product_service->product_name;?>" name="product_name" class="form-control"/>
                    </div>
                </div>
                <div class="form-group required">
                <div class="col-md-7">
                        <label class="control-label">Spesification</label>
                        <input type="text" id="spesification" value="<?php echo $product_service->spesification;?>" name="spesification" class="form-control"/>
                    </div>
                </div>
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Submit </button>
                {!! Form::close() !!}
    </form>
</div>