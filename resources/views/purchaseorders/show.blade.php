<div class="col-md-12">
   <h4><b>Purchase Order</b></h4>
   <h3><a href="{{route('purchaseorder.index')}}" data-toggle="tooltip" title="Back To Grid" ><span class="glyphicon glyphicon-arrow-left"></span></a></h3>
</div>

<form class="form-horizontal" method="POST" action="{{ route('purchaseorder.getsave','4') }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="form-group required">
        <label class="col-sm-2 control-label" hidden="true">counterx</label>
        <div class="col-sm-6">
            <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-sm-5">
                <label class="control-label">PO Number</label>
                <input type="label" id="po_number" value="<?php echo $purchase_order->po_number;?>" name="po_number" class="form-control" readonly ="true"/>
            </div> 

            <div class="col-sm-5">
                <label class="control-label">External Provider</label>  
                <input type="text" id="external_provider_id" value="<?php echo  $purchase_order->externalprovider->expname;?>" name="external_provider_id" class="form-control"  readonly ="true"/>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-5">
             <label class="control-label">Date</label>
             <input type="date"  value="<?php echo $purchase_order->date;?>" id="date" name="date" class="form-control" readonly ="true"/>
         </div>

         <div class="required">
             <div class="col-sm-5">
                <label class="control-label">Term of Payment</label>
                <input type="text" id="term" value="<?php echo $purchase_order->term;?>" name="term" class="form-control" readonly ="true"/>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group required">
        <div class="col-sm-5">
            <label class="control-label">Attention</label>
            <input type="text" id="attention" value="<?php echo $purchase_order->attention;?>" name="attention" class="form-control" readonly ="true"/>
        </div>

        <div class="required">
            <div class="col-md-5">
                <label class="control-label">Reff</label>

                <input type="text" id="reff" value="<?php echo $purchase_order->reff;?>" name="reff" class="form-control" readonly ="true"/>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group required">
        <div class="col-md-10">
            <label class="control-label" id="demo">Spesial Instruction</label>
            <input type="text" id="spesial_instruction" value="<?php echo $purchase_order->spesial_instruction;?>" name="spesial_instruction" class="form-control" readonly ="true"/>
        </div>
    </div>

    <div id="data-purchaseorder"></div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="TData">
            <thead>
                <tr>
                    <td colspan="7">
                        <label># Details Of Purchase Order</label>
                    </td> 
                </tr>
                <tr>
                    <th style="text-align:left;">Product Services</th>
                    <th style="text-align:center;">Qty</th>
                    <th style="text-align:center;">Uom</th>
                    <th style="text-align:center;">Unit Price</th>
                    <th style="text-align:center;">Amount</th>
                </tr>

            </thead>
            <tbody>
                <?php $v = 0; foreach($purchase_order->purchaseOrderDetail as $purchase_order_detail): ?>
                <tr class="index" id="<?php echo $v;?>">
                    <td style="text-align:Left;"> <?php echo $purchase_order_detail->productService->product_name;?> </td>
                    <td style="text-align:center;">  <?php echo $purchase_order_detail->qty;?> </td>
                    <td style="text-align:center;">  <?php echo $purchase_order_detail->uom->name;?> </td>
                    <td style="text-align:right;">  <?php echo number_format($purchase_order_detail->unit_price,0,",",".") ?> </td>
                    <td style="text-align:right;">  <?php echo number_format($purchase_order_detail->amount,0,",",".") ?> </td>
                </tr>
                <?php $v++; EndForeach; ?>
            </tbody>
        </table>
        <table>
            <td style="text-align:right;"> Total Amount : <?php echo number_format($purchase_order_detail_amount,0,",",".");?> </td>
        </table>
    </div>
</form>