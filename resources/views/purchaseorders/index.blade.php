<head>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
</head>
<div class="col-md-12">
    <h4><b>Purchase Orders List</b></h4>
</div>

<form method="GET" href="{{route('purchaseorder.index')}}">
    <div class="col-md-9">
        <div class="col-md-3">
            <label>First Date</label>
            <input type="date" name="date_first" placeholder="First Date" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">
        </div>
        <div class="col-md-3">
            <label>Last Date</label> 
            <div class="input-group">    
                <input type="date" name="date_last" placeholder="Last Date" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control">  
                <span class="input-group-btn">         
                    <button type="submit" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>  
    </div>
</form>
<br>
<?php 
$no = 1; 
if(isset($_GET['page'])){
    $no = ($_GET['page']*10)-9;
}
?>
@if (Session::has('message'))

<div class="alert alert-success">
    <i class="fa fa-exclamation-circle"></i><small>  {{Session::get('message')}} !!</small>
    <button type="button" class="close" data-dismiss="alert">
        ×
    </button>
</div>
@endif
<br>
<table class="table table-striped table-hover" id="TBLpurchaseorder">
<thead>
    <tr>
        <tr>
            <th colspan="11">
                <h3 class="pull-right">
                    <a style="color: green" href="{{ route('purchaseorder.create') }}">
                        <span class="glyphicon glyphicon-plus" class="pull-right"></span>
                    </a>
                </h3>
            </th>
        </tr>
        <th style="text-align:center;">#</th>
        <th>PO Source</th>
        <th>PO Number</th>
        <th>External Provider</th>
        <th>Date</th>
        <th>Term</th>
        <th>Attention</th>
        <th>Reff</th>
        <th>Spesial Instruction</th>
        <th style="text-align:center;">Total Amount</th>
        <th style="text-align:center;">Action</th>
    </tr>
    </thead>
    <tbody>
        <!-- <?php $i=0; ?> -->
        <?php foreach($data as $row): ?>
            <script type="text/javascript">
                function gettotals(idx, x)
                {
                    var x = x+1;
                    var purchase_order_id = idx;
                    $.ajax({
                        type:'get',
                        url: '<?php echo url('/purchaseorders/totals');?>',
                        data:{'id':purchase_order_id},
                        datatype:'json',
                        success:function(data){
                            var s = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            document.getElementById("grandtotal"+x).innerHTML = s;
                        },
                        error:function(){
                        }
                    });
                }
            </script>

            <script type="text/javascript"> 
                gettotals({{$row->id}}, {{ $i }}); 
            </script>

            <tr>
                <!-- {{++$i}}   -->
                <td><?php echo $no;?></td>
                <td><?php echo $row->po_source;?></td>
                <td><?php echo $row->po_number;?></td>
                <td><?php echo $row->externalprovider->expname;?></td>
                <td><?php echo $row->date;?></td>
                <td><?php echo $row->term;?></td>
                <td><?php echo $row->attention;?></td>
                <td><?php echo $row->reff;?></td>
                <td><?php echo $row->spesial_instruction;?></td>
                <td style="text-align:right;"><text id ='grandtotal{{ $i }}' > </text> </td>
                <td>
                    <a href="{{ route('purchaseorder.edit',$row->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{{ route('purchaseorder.show',$row->id) }}"><i class="glyphicon glyphicon-search"></i></a>
                    <a style="color: red" href="{{ route('purchaseorder.destroy',$row->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
                </td>
            </tr>
            <?php $no++; EndForeach; ?>
        </tbody>
    </table>

<script type="text/javascript">
    $(document).ready( function () {
        $('#TBLpurchaseorder').DataTable();
    } );
</script>
