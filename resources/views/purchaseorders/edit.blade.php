<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"</script>
    <script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/bootstrap/less/bootstrap.less') }}">
    <script src="{{ asset('js/bootstrap/less-1.7.4.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/summernote/summernote.css') }}">
    <script src="{{ asset('js/summernote/summernote.js') }}"</script> 
    <script src="{{ asset('js/jquery/datetimepicker/moment.js') }}"</script>
    <script src="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.js') }}"</script>
    <link rel="stylesheet" href="{{ asset('js/jquery/datetimepicker/bootstrap-datetimepicker.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
    <script src="{{ asset('js/jquery/select2.js') }}"</script>
    <script src="{{ asset('js/jquery/date.js') }}"</script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            ]) !!};
        </script>
</head>

<div class="col-lg-12">
    <h4><b>Purchase Order</b></h4>
    <h3><a href="{{ route('purchaseorder.index') }}"><span class="glyphicon glyphicon-arrow-left"></span></a></h3>
</div>

<form class="form-horizontal" method="POST" action="{{ route('purchaseorder.getsave','4') }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="form-group required">
        <label class="control-label" hidden="true">counterx</label>
        <input type="hidden" id="counterx" name="counterx" class="form-control" readonly="true" />
    </div>

    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-5">
                <label class="control-label">id</label>
                <input type="text" id="id" value="<?php echo $purchase_order->id;?>" name="id" class="form-control" readonly="true"/>
            </div>

            <div class="col-md-5">
                <label class="control-label">PO Source</label> 
                <input type="text" id="po_source" value="<?php echo $purchase_order->po_source;?>" name="po_source" class="form-control" required/>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-5">
                <label class="control-label">PO Number</label>
                <input type="text" id="po_number" value="<?php echo $purchase_order->po_number;?>" name="po_number" class="form-control" required/> 
            </div>

            <div class="col-md-5">
                <label class="control-label">External Provider</label>
                <select name="external_provider_id" id="external_provider_id" class="form-control">
                    <option></option>
                    <?php foreach($external_providers as $c): ?>
                        <?php if($c->id==$purchase_order->external_provider_id): ?>
                            <option value="<?php echo $c->id;?>" selected><?php echo $c->expname;?></option>
                            <?php Else: ?>
                            <option value="<?php echo $c->id;?>"><?php echo $c->expname;?></option>
                        <?php EndIf; ?>     
                    <?php EndForeach; ?>
                </select>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-5">
                 <label class="control-label">Date</label>
                 <input type="date"  value="<?php echo $purchase_order->date;?>" id="date" name="date" class="form-control" required/>
             </div>

             <div class="required">
                 <div class="col-md-5">
                    <label class="control-label">Term of Payment</label>
                    <input type="text" id="term" value="<?php echo $purchase_order->term;?>" name="term" class="form-control"/>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-5">
                <label class="control-label">Attention</label>
                <input type="text" id="attention" value="<?php echo $purchase_order->attention;?>" name="attention" class="form-control"/>
            </div>

            <div class="required">
                <div class="col-md-5">
                    <label class="control-label">Reff</label>
                    <input type="text" id="reff" value="<?php echo $purchase_order->reff;?>" name="reff" class="form-control"/>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group required">
            <div class="col-md-10">
                    <label class="control-label" id="demo">Spesial Instruction</label>
                    <input type="text" id="spesial_instruction" value="<?php echo $purchase_order->spesial_instruction;?>" name="spesial_instruction" class="form-control"/>
            </div>

        </div>
    </div>

    <div id="data-purchaseorder"></div>
    
    <table class="table table-bordered table-hover" id="TData">
        <tr>
            <td colspan="7">
                <label># Details Of Purchase Order</label>
                <h3><a href="javascript:void(0)" onclick="AddPoDetil()" title="Add New Purchase Order Detil" style="color: green ">
                <span class="glyphicon glyphicon-plus pull-right"></span>
                </a></h3>
            </td>
        </tr>
        <tr>
            <th>Product Service</th>
            <th style="text-align:center;">Qty</th>
            <th style="text-align:center;">Uom</th>
            <th style="text-align:center;">Unit Price</th>
            <th style="text-align:center;">Amount</th>
            <th style="text-align:center;">Action</th>
        </tr>

        <script type="text/javascript">
            var idx=0;
        </script>

        <?php $v = 0; foreach($purchase_order_detail as $data_detail): ?>

        <tr class="index" id="<?php echo $v;?>">
        <td><select id="product_service_id<?php echo $v;?>" name="product_service_id<?php echo $v;?>" class="form-control">
            <option></option>
            <?php foreach($product_services as $c): ?>
                <?php if($c->id==$data_detail->product_service_id): ?>
                    <option value="<?php echo $c->id;?>" selected><?php echo $c->product_name;?></option>
                    <?php Else: ?>
                    <option value="<?php echo $c->id;?>"><?php echo $c->product_name;?></option>
                <?php EndIf; ?>     
            <?php EndForeach; ?>
        </select></td>

        <td><input type="text" value="<?php echo $data_detail->qty;?>" id="qty<?php echo $v;?>" name="qty<?php echo $v;?>" class="form-control" /></td>

        <td>
            <select name="uom_id<?php echo $v;?>" id="uom_id<?php echo $v;?>" class="form-control">
                <option></option>
                <?php foreach($uoms as $c): ?>
                    <?php if($c->id==$data_detail->uom_id): ?>
                        <option value="<?php echo $c->id;?>" selected><?php echo $c->name;?></option>
                        <?php Else: ?>
                        <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                    <?php EndIf; ?>     
                <?php EndForeach; ?>
            </select>
        </td>

        <td><input type="text" style="text-align:right;" value="<?php echo $data_detail->unit_price;?>" id="unit_price<?php echo $v;?>" name="unit_price<?php echo $v;?>" class="form-control" /></td>

        <td><input type="text" style="text-align:right;" value="<?php echo number_format($data_detail->amount,0,",",".");?>" id="amount<?php echo $v;?>" class="form-control" readonly="true" /></td>

        <script type="text/javascript">
            var nm = $('#v_jv').val()
            $('#product_service_id'+idx).select2({
                ajax: {
                    placeholder: 'Product Services',    
                    url: '<?php echo url('/purchaseorders/getproductservice');?>',
                    dataType: 'json',
                    quietMillis: 100,
                    data: function (term) {
                        return {
                    q: term, // search term
                };
            },
            results: function (data) {
                var myResults = [];
                $.each(data, function (index, item) {
                    myResults.push({
                        'id': item.id,
                        'text': item.text
                    });
                });
                return {
                    results: myResults
                };
            },
            minimumInputLength: 3
        }
        });
            $('#uom_id'+idx).select2({
                ajax: {
                    placeholder: 'Uom',    
                    url: '<?php echo url('/purchaseorders/getuom');?>',

                    dataType: 'json',
                    quietMillis: 100,
                    data: function (term) {
                        return {
                            q: term, // search term
                        };
                    },
                    results: function (data) {
                        var myResults = [];
                        $.each(data, function (index, item) {
                            myResults.push({
                                'id': item.id,
                                'text': item.text
                            });
                        });
                        return {
                            results: myResults
                        };
                    },
                    minimumInputLength: 3
                }
            });
        </script>

        <td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)"><i class="glyphicon glyphicon-trash"></i></a></td>

        <script type="text/javascript">
            idx=idx+1;
        </script>
        </tr>

        <?php $v++; EndForeach; ?>

    </table>
    <div class="pull-right">
        <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><span class="glyphicon glyphicon-refresh"></span></button>
        <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-saved"></span></button>
    </div>
</form>

<script type="text/javascript">
    if (idx<=0) {idx=0} else {}
        $(document).ready(function(){
        });

    function AddPoDetil(){
        var row = $('#TData tbody tr').length;
        if(row>0){
            var index = $('.index').last().attr('id');
            index = parseInt(index);
            row = parseInt(row);
            row = index+1;
        }

        $('#counterx').val(idx);

        document.getElementById("counterx").innerHTML=idx;
        var x = document.getElementById("counterx");
        var html = '<tr id="'+idx+'" class="index">';
        html += '<td><input type="hidden" id="product_service_id'+row+'" name="product_service_id'+idx+'"  class="form-control" required/></td>'; 
        html += '<td><input type="text" id="qty'+row+'" name="qty'+idx+'" onChange="javascript:famount(idx)" class="form-control" /></td>';
        html += '<td><input type="hidden" id="uom_id'+row+'" name="uom_id'+idx+'" class="form-control" required/></td>'; 
        html += '<td><input type="text" id="unit_price'+row+'" name="unit_price'+idx+'" onChange="javascript:famount(idx)" class="form-control" /></td>';
        html += '<td><input type="text" id="amount'+row+'" name="amount'+idx+'" class="form-control" readonly="true"/></td>';
        html += '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="glyphicon glyphicon-trash"><i class="fa fa-times"></i></a></td>';
        html += '</tr>';
        $('#TData tbody').append(html);

        idx=idx+1;
        $('#counterx').val(idx);
        $('#product_service_id'+row).select2({
            ajax: {
                placeholder: 'Product Services',    
                url: '<?php echo url('/purchaseorders/getproductservice');?>',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });
        $('#uom_id'+row).select2({
            ajax: {
                placeholder: 'Uom',    
                url: '<?php echo url('/purchaseorders/getuom');?>',
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        q: term, // search term
                    };
                },
                results: function (data) {
                    var myResults = [];
                    $.each(data, function (index, item) {
                        myResults.push({
                            'id': item.id,
                            'text': item.text
                        });
                    });
                    return {
                        results: myResults
                    };
                },
                minimumInputLength: 3
            }
        });
    }

    function deleteRow(btn) {
      var row = btn.parentNode.parentNode;
      row.parentNode.removeChild(row);
      idx=idx-1;
      if (idx<=0) {idx=0} else {}
          $('#counterx').val(idx);
    }

    function famount(idx) {
        var prmqty = 0;
        var prmunit_price = 0;
        var prmamount=0;
        var idfamount;

        idfx=idx-1;
        prmqty=$('#qty'+idfx).val();
        prmunit_price=$('#unit_price'+idfx).val();
        prmamount=prmqty * prmunit_price;
        $('#amount'+idx).val(prmamount);
    }

    $('#counterx').val(idx); 
    // AddPoDetil();
    //uom();

</script>