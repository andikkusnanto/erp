<?php
    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home.default');
    Route::get('/', 'HomeController@index')->name('home.root');

    Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'uoms'], function () {
            Route::get('/', 'UomController@index')->name('uom.index');
            Route::get('/create','UomController@create')->name('uom.create');
            Route::post('/store','UomController@store')->name('uom.store');
            Route::get('/search', 'UomController@search')->name('uom.search');
            Route::get('/{uoms}/edit', 'UomController@edit')->name('uom.edit');
            Route::PATCH('/{uoms}/update', 'UomController@update')->name('uom.update');
            Route::get('/{uoms}/show', 'UomController@show')->name('uom.show');
            Route::get('/{uoms}/destroy', 'UomController@destroy')->name('uom.destroy');
            });
    });

    Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'externalproviders'], function () {
            Route::get('/', 'ExternalProviderController@index')->name('externalprovider.index');
            Route::get('/create','ExternalProviderController@create')->name('externalprovider.create');
            Route::post('/store','ExternalProviderController@store')->name('externalprovider.store');
            Route::get('/search', 'ExternalProviderController@search')->name('externalprovider.search');
            
            Route::get('/{externalproviders}/show', 'ExternalProviderController@show')->name('externalprovider.show');
            Route::get('/{externalproviders}/edit', 'ExternalProviderController@edit')->name('externalprovider.edit');
            Route::PATCH('/{externalproviders}/update', 'ExternalProviderController@update')->name('externalprovider.update');
            Route::get('/{externalproviders}/destroy', 'ExternalProviderController@destroy')->name('externalprovider.destroy');
            });
    });

    Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'products'], function () {
            Route::get('/{products}/edit', 'ProductController@edit')->name('product.edit');
            Route::PATCH('/{products}/update', 'ProductController@update')->name('product.update');
            Route::get('/{products}/show', 'ProductController@show')->name('product.show');
            Route::get('/{products}/destroy', 'ProductController@destroy')->name('product.destroy');
            Route::get('/create','ProductController@create')->name('product.create');
            Route::post('/store','ProductController@store')->name('product.store');
            Route::get('/index', 'ProductController@index')->name('product.index');
            Route::get('/search', 'ProductController@search')->name('product.search');
            Route::get('/getproducttype','ProductController@getProductType')->name('product.getproducttype');
            Route::post('getupdate', 'ProductController@getUpdate')->name('product.getupdate');
            });
    });
 
    Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'producttypes'], function () {
            Route::get('/{producttypes}/edit', 'ProductTypeController@edit')->name('producttype.edit');
            Route::PATCH('/{producttypes}/update', 'ProductTypeController@update')->name('producttype.update');
            Route::get('/{producttypes}/show', 'ProductTypeController@show')->name('producttype.show');
            Route::get('/{producttypes}/destroy', 'ProductTypeController@destroy')->name('producttype.destroy');
            Route::get('/create','ProductTypeController@create')->name('producttype.create');
            Route::post('/store','ProductTypeController@store')->name('producttype.store');
            Route::get('/index', 'ProductTypeController@index')->name('producttype.index');
            Route::get('/search', 'ProductTypeController@search')->name('producttype.search');
            Route::post('getsave', 'ProductTypeController@getSave')->name('producttype.getsave');
            Route::PATCH('getupdate', 'ProductTypeController@getUpdate')->name('producttype.getupdate');
            });
    });
 
    Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'purchaseorders'], function () {
            Route::get('/{purchaseorders}/edit', 'PurchaseOrderController@edit')->name('purchaseorder.edit');
            Route::PATCH('/{purchaseorders}/update', 'PurchaseOrderController@update')->name('purchaseorder.update');
            Route::get('/{purchaseorders}/show', 'PurchaseOrderController@show')->name('purchaseorder.show');
            Route::get('/totals', 'PurchaseOrderController@totals')->name('purchaseorder.totals');
            Route::get('/{purchaseorders}/destroy', 'PurchaseOrderController@destroy')->name('purchaseorder.destroy');
            Route::get('/create','PurchaseOrderController@create')->name('purchaseorder.create');
            Route::post('/store','PurchaseOrderController@store')->name('purchaseorder.store');
            Route::get('/index','PurchaseOrderController@index')->name('purchaseorder.index');
            Route::get('/financialrepaort','PurchaseOrderController@fincancialReport')->name('purchaseorder.financialreport');
            Route::get('/search','PurchaseOrderController@search')->name('purchaseorder.search');
            Route::get('/getproductservice','PurchaseOrderController@getProductService')->name('purchaseorder.getproductservice');
            Route::get('/getuom','PurchaseOrderController@getUom')->name('purchaseorder.getuom');
            Route::get('/getexternalprovider','PurchaseOrderController@getExternalProvider')->name('purchaseorder.getexternalprovider');
            Route::post('getsave', 'PurchaseOrderController@getSave')->name('purchaseorder.getsave');
            });
    }); 

    Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'purchaseorderdetils'], function () {
            Route::delete('/{purchaseorderdetils}/destroy', 'PurchaseOrderDetilController@destroy')->name('purchaseorderdetil.destroy');
        });
    }); 

    Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'deliveryorders'], function () {
            Route::get('/{deliveryorders}/edit', 'DeliveryOrderController@edit')->name('deliveryorder.edit');
            Route::PATCH('/{deliveryorders}/update', 'DeliveryOrderController@update')->name('deliveryorder.update');
            Route::get('/{deliveryorders}/show', 'DeliveryOrderController@show')->name('deliveryorder.show');
            Route::get('/{deliveryorders}/destroy', 'DeliveryOrderController@destroy')->name('deliveryorder.destroy');
            Route::get('/create','DeliveryOrderController@create')->name('deliveryorder.create');
            Route::post('/store','DeliveryOrderController@store')->name('deliveryorder.store');
            Route::get('/index','DeliveryOrderController@index')->name('deliveryorder.index');
            Route::get('/search','DeliveryOrderController@search')->name('deliveryorder.search');
            Route::get('/getpurchaseorder','DeliveryOrderController@getPurchaseOrder')->name('deliveryorder.getpurchaseorder');
            Route::get('/getuom','DeliveryOrderController@getUom')->name('deliveryorder.getuom');
            Route::get('/getuomname','DeliveryOrderController@getUomName')->name('deliveryorder.getuomname');
            Route::get('/getexternalprovider','DeliveryOrderController@getExternalProvider')->name('deliveryorder.getexternalprovider');
            Route::post('getsave', 'DeliveryOrderController@getSave')->name('deliveryorder.getsave');
            });
    }); 

    Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'invoices'], function () {
            Route::get('/{invoices}/edit', 'InvoiceController@edit')->name('invoice.edit');
            Route::PATCH('/{invoices}/update', 'InvoiceController@update')->name('invoice.update');
            Route::get('/{invoices}/show', 'InvoiceController@show')->name('invoice.show');
            Route::get('/{invoices}/destroy', 'InvoiceController@destroy')->name('invoice.destroy');
            Route::get('/create','InvoiceController@create')->name('invoice.create');
            Route::post('/store','InvoiceController@store')->name('invoice.store');
            Route::get('/index','InvoiceController@index')->name('invoice.index');
            Route::get('/search','InvoiceController@search')->name('invoice.search');
            Route::get('/totals', 'InvoiceController@totals')->name('invoice.totals');
            Route::get('/getdeliveryorder','InvoiceController@getDeliveryOrder')->name('invoice.getdeliveryorder');
            Route::get('/getuom','InvoiceController@getUom')->name('invoice.getuom');
            Route::get('/getexternalprovider','InvoiceController@getExternalProvider')->name('invoice.getexternalprovider');
            Route::post('getsave', 'InvoiceController@getSave')->name('invoice.getsave');
            });
    }); 
    
    Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'paymentvouchers'], function () {
            Route::get('/{paymentvouchers}/edit', 'paymentvoucherController@edit')->name('paymentvoucher.edit');
            Route::PATCH('/{paymentvouchers}/update', 'paymentvoucherController@update')->name('paymentvoucher.update');
            Route::get('/{paymentvouchers}/show', 'paymentvoucherController@show')->name('paymentvoucher.show');
            Route::get('/{paymentvouchers}/destroy', 'paymentvoucherController@destroy')->name('paymentvoucher.destroy');
            Route::get('/create','paymentvoucherController@create')->name('paymentvoucher.create');
            Route::post('/store','paymentvoucherController@store')->name('paymentvoucher.store');
            Route::get('/index','paymentvoucherController@index')->name('paymentvoucher.index');
            Route::get('/search','paymentvoucherController@search')->name('paymentvoucher.search');
            Route::get('/totals', 'paymentvoucherController@totals')->name('paymentvoucher.totals');
            Route::get('/getinvoice','paymentvoucherController@getInvoice')->name('paymentvoucher.getinvoice');
            Route::get('/getuom','paymentvoucherController@getUom')->name('paymentvoucher.getuom');
            Route::get('/getexternalprovider','paymentvoucherController@getExternalProvider')->name('paymentvoucher.getexternalprovider');
            Route::post('getsave', 'paymentvoucherController@getSave')->name('paymentvoucher.getsave');
            });
    }); 

    Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'reports'], function () {
            Route::get('/financialreport','ReportController@financialReport')->name('report.financialreport');
            });
    }); 

    Route::group(['middleware' => ['auth']], function() {
    Route::group(['prefix' => 'appmenus'], function () {
            Route::get('/{appmenus}/edit', 'AppMenuController@edit')->name('appmenu.edit');
            Route::PATCH('/{appmenus}/update', 'AppMenuController@update')->name('appmenu.update');
            Route::get('/{appmenus}/show', 'AppMenuController@show')->name('appmenu.show');
            Route::get('/{appmenus}/destroy', 'AppMenuController@destroy')->name('appmenu.destroy');
            Route::get('/create','AppMenuController@create')->name('appmenu.create');
            Route::post('/store','AppMenuController@store')->name('appmenu.store');
            Route::get('/index', 'AppMenuController@index')->name('appmenu.index');
            Route::get('/search', 'AppMenuController@search')->name('appmenu.search');
            Route::get('/getproducttype','AppMenuController@getProductType')->name('appmenu.getproducttype');
            Route::post('getsave', 'AppMenuController@getSave')->name('appmenu.getsave');
            });
    });

        Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'userroles'], function () {
            Route::get('/{userroles}/edit', 'UserRoleController@edit')->name('userrole.edit');
            Route::PATCH('/{userroles}/update', 'UserRoleController@update')->name('userrole.update');
            Route::get('/{userroles}/show', 'UserRoleController@show')->name('userrole.show');
            Route::get('/{userroles}/destroy', 'UserRoleController@destroy')->name('userrole.destroy');
            Route::get('/create','UserRoleController@create')->name('userrole.create');
            Route::post('/store','UserRoleController@store')->name('userrole.store');
            Route::get('/index','UserRoleController@index')->name('userrole.index');
            Route::get('/search','UserRoleController@search')->name('userrole.search');
            Route::get('/getappmenu','UserRoleController@getAppMenu')->name('userrole.getappmenu');
            Route::post('getsave', 'UserRoleController@getSave')->name('userrole.getsave');
            });
    }); 

        Route::group(['middleware' => ['auth']], function() {
        Route::group(['prefix' => 'userrolemans'], function () {
            Route::get('/{userrolemans}/edit', 'UserRoleManController@edit')->name('userroleman.edit');
            Route::PATCH('/{userrolemans}/update', 'UserRoleManController@update')->name('userroleman.update');
            Route::get('/{userrolemans}/show', 'UserRoleManController@show')->name('userroleman.show');
            Route::get('/{userrolemans}/destroy', 'UserRoleManController@destroy')->name('userroleman.destroy');
            Route::get('/create','UserRoleManController@create')->name('userroleman.create');
            Route::post('/store','UserRoleManController@store')->name('userroleman.store');
            Route::get('/index','UserRoleManController@index')->name('userroleman.index');
            Route::get('/search','UserRoleManController@search')->name('userroleman.search');
            Route::get('/getappMenu','UserRoleManController@getAppMenu')->name('userroleman.getappmenu');
            Route::get('/getUserMenu','UserRoleManController@getUserMenu')->name('userroleman.getusermenu');
            Route::post('getsave', 'UserRoleManController@getSave')->name('userroleman.getsave');
            });
    }); 