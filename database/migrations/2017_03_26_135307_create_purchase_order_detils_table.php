<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderDetilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('purchase_order_detils', function (Blueprint $table) {
            $table->increments('id');
            $table->string('po_number');
            $table->string('acc_number');
            $table->string('product_code');
            $table->double('qty');
            $table->string('curr');
            $table->double('unit_prise');
            $table->integer('purchase_orders_id');
            $table->integer('uom_id');
            $table->integer('product_services_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_detils');
    }
}
