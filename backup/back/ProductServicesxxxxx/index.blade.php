@extends('layouts.app')
    
@section('content')
<h1>Product & Services</h1>
    <a class="btn btn-success" href="{{ route('DeliveryOrders.create') }}"> Create New Item</a>

    @if ($message = Session::get('success'))

            <p>{{ $message }}</p>

    @endif

<section class="col-lg-6"></section>
    <div class="table-responsive">
        <div class="page-header">
            <table class="table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Material Type</th>
                        <th>Spesification</th>
                        <th>Min Order</th>
                        <th>Min Qty</th>
                        <th width="210">Action</th>
                    </tr>

                    @foreach ($itemsx as $key => $item)

                    <!-- show table from database !-->
                    <tr>
                        <td align="center">{{ ++$b }}</td>
                        <td>{{ $item->productcode }}</td>
                        <td>{{ $item->productname }}</td>
                        <td>{{ $item->materialtype }}</td>
                        <td>{{ $item->spesification }}</td>
                        <td>{{ $item->minorder }}</td>
                        <td>{{ $item->minqty }}</td>
                  
                        <td>
                            <a class="btn btn-info" href="{{ route('ProductServices.show',$item->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('ProductServices.edit',$item->id) }}">Edit</a>
                            <!-- button delete form !-->
                            {!! Form::open(['method' => 'DELETE','route' => ['ProductServices.destroy', $item->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>

                    @endforeach

                </table>
            
        </div>
    </div>
<!-- parsing html element to send all data !-->
    <div class="page"> {!! $itemsx->render() !!}</div>
@endsection